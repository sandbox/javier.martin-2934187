!function($) {
  $(document).ready(function() {

	  function doit_xlsx(type, fn, dl) {
		  var elt = document.getElementById('signings-table');
		  var wb = XLSX.utils.table_to_book(elt, {sheet:"Sheet JS"});
		  return dl ?
				  XLSX.write(wb, {bookType:type, bookSST:true, type: 'base64'}) :
				  XLSX.writeFile(wb, fn || ('test.' + (type || 'xlsx')));
	  }
	  
	  function doit_pdf() {
		  var doc = new jsPDF();
		    // You can use html:
		    doc.autoTable({
		    	html: '#signings-table',
		    	tableWidth: 'wrap',
    		});
		    
		    doc.save('table.pdf');
	  }
	  
	  $('#button-xlsx').on('click', function() { doit_xlsx('xlsx'); } );
	  $('#button-pdf').on('click', function() { doit_pdf(); } );
	  
  });
}(jQuery);