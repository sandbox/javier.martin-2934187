<?php

namespace Drupal\efichajes\Controller;

use Drupal\Core\Session\AccountProxyInterface;
use Drupal\user\UserInterface;
use Drupal\Core\Access\AccessResult;

class AccessController {
  
  /**
   * Custom access used to check all user only access to his signings data,
   * except users with 'efichajes admin' permission.
   * @param AccountProxyInterface $account
   * @param UserInterface $user
   * @return \Drupal\Core\Access\AccessResultAllowed|\Drupal\Core\Access\AccessResultForbidden
   */
  public function ownWorkerAccess (AccountProxyInterface $account, UserInterface $user) {
    if ( isset($user) && 
      ($account->id() == $user->id()) && 
      ($user->hasPermission('efichajes worker')) ) {
      return AccessResult::allowed();
      } else if ($account->hasPermission('efichajes admin')) {
        return AccessResult::allowed();
      }
    
    return AccessResult::forbidden('Access not allowed');
  }
  
}