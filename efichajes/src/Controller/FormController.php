<?php

namespace Drupal\efichajes\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\user\UserInterface;
use Drupal\Core\Form\FormBuilderInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\node\NodeInterface;

class FormController extends ControllerBase {
  protected $form_builder;
  
  /**
   * Implement construct method.
   * @param FormBuilderInterface $form_builder
   */
  public function __construct(FormBuilderInterface $form_builder) {
    $this->form_builder = $form_builder;
  }
  
  /**
   * Implement create method.
   * @param ContainerInterface $container
   * @return \Drupal\efichajes\Controller\FormController
   */
  public static function create (ContainerInterface $container) {
    return new static (
      $container->get('form_builder')
    );
  }
  
  /**
   * Manage form returned by operation
   * @param string $operation
   * @param NodeInterface $node
   * @return NULL|array
   */
  public function SigningTypesForms(string $operation, NodeInterface $node = null) {
    switch ($operation) {
      case 'alter':
        $form = $this->form_builder->getForm('\Drupal\efichajes\Form\ManageSigningTypeForm', $operation, $node);
        break;
      case 'add':
        $form = $this->form_builder->getForm('\Drupal\efichajes\Form\ManageSigningTypeForm', $operation);
        break;
      case 'delete':
        $form = $this->form_builder->getForm('\Drupal\efichajes\Form\ConfirmDeleteSigningTypeForm', $node);
        break;
      default:
        $form = $this->form_builder->getForm('\Drupal\efichajes\Form\SigningTypesForm');
    }
    
    return $form;
  }
  
  public function WorkersForms(string $operation, UserInterface $user = null) {
    switch ($operation) {
      case 'alter':
        $form = $this->form_builder->getForm('\Drupal\efichajes\Form\ManageWorkerForm', $operation, $user);
        break;
      case 'add':
        $form = $this->form_builder->getForm('\Drupal\efichajes\Form\ManageWorkerForm', $operation);
        break;
      case 'delete':
        $form = $this->form_builder->getForm('\Drupal\efichajes\Form\ConfirmDeleteWorker', $user);
        break;
      default:
        $form = $this->form_builder->getForm('\Drupal\efichajes\Form\WorkersForm');
        break;
    }
    
    return $form;
  }
  
  public function WorkCalendarsForms(string $operation, NodeInterface $node = null) {
    switch($operation) {
      case 'add':
        $form = $this->form_builder->getForm('\Drupal\efichajes\Form\ManageWorkCalendarForm', $operation);
        break;
      case 'alter':
        $form = $this->form_builder->getForm('\Drupal\efichajes\Form\ManageWorkCalendarForm', $operation, $node);
        break;
      case 'delete':
        $form = $this->form_builder->getForm('\Drupal\efichajes\Form\ConfirmDeleteWorkCalendar', $node);
        break;
      default:
        $form = $this->form_builder->getForm('\Drupal\efichajes\Form\WorkCalendarsForm');
        break;
    }
    
    return $form;
  }
  
  public function RangeDateForms(NodeInterface $node_wc = null, string $operation, NodeInterface $node_rd = null) {
    switch($operation) {
      case 'add':
        $form = $this->form_builder->getForm('\Drupal\efichajes\Form\ManageRangeDateForm', $node_wc, $operation);
        break;
      case 'alter':
        $form = $this->form_builder->getForm('\Drupal\efichajes\Form\ManageRangeDateForm', $node_wc, $operation, $node_rd);
        break;
      case 'delete':
        $form = $this->form_builder->getForm('\Drupal\efichajes\Form\ConfirmDeleteRangeDateForm', $node_wc, $node_rd);
        break;
      default:
        $form = $this->form_builder->getForm('\Drupal\efichajes\Form\WorkCalendarsForm');
        break;
    }
    
    return $form;
  }
}