<?php

namespace Drupal\efichajes\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\user\UserInterface;

class ChangePassword extends FormBase {
  protected $current_user;
  protected $entityTypeManager;
  protected $logger;
  protected $user;
  
  /**
   * Construct implementation.
   * @param AccountProxyInterface $current_user
   * @param EntityTypeManagerInterface $entityTypeManager
   * @param LoggerChannelFactoryInterface $logger
   */
  public function __construct(AccountProxyInterface $current_user,
      EntityTypeManagerInterface $entityTypeManager, LoggerChannelFactoryInterface $logger) {
    $this->current_user = $current_user;
    $this->entityTypeManager = $entityTypeManager;
    $this->logger = $logger->get('efichajes');
  }
  
  /**
   * Create implementation.
   * @param ContainerInterface $container
   * @return \Drupal\efichajes\Form\ChangePassword
   */
  public static function create(ContainerInterface $container) {
    return new static (
      $container->get('current_user'),
      $container->get('entity_type.manager'),
      $container->get('logger.factory')
    );
  }
  
  /**
   * 
   * {@inheritDoc}
   * @see \Drupal\Core\Form\FormInterface::getFormId()
   */
  public function getFormId() {
    return 'efichajesChangePasswordForm';
  }
  
  /**
   * 
   * {@inheritDoc}
   * @see \Drupal\Core\Form\FormBase::validateForm()
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    if (!isset($this->user)) {
      $form_state->setErrorByName('user', $this->t('User not found.'));
    }
  }
  
  /**
   * 
   * {@inheritDoc}
   * @see \Drupal\Core\Form\FormInterface::buildForm()
   */
  public function buildForm(array $form, FormStateInterface $form_state, UserInterface $user = null) {
    $this->user = $user;
    
    $form['form_description'] = [
      '#markup' => $this->t('Use this form to change your password.'),
    ];
    
    $form['password'] = [
      '#type' => 'password_confirm',
      '#description' => $this->t('Keep this field empty to maintain password.'),
      '#required' => TRUE,
    ];
    
    $form['actions'] = [
      '#type' => 'actions',
    ];
    
    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Submit'),
    ];
    
    return $form;
  }
  
  /**
   * 
   * {@inheritDoc}
   * @see \Drupal\Core\Form\FormInterface::submitForm()
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $password = $form_state->getValue('password');
    
    if (isset($this->user)) {
      $this->user->setPassword($password);
      $this->user->save();
      
      drupal_set_message($this->t('Password changed.'));
      
      $this->logger->info('@login - ChangePassword - Password changed for @userworker.', [
        '@login' => $this->current_user->getUsername(),
        '@userworker' => $this->user->getUsername(),
      ]);
    }
  }
}