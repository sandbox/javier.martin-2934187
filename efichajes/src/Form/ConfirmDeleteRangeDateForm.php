<?php 

namespace Drupal\efichajes\Form;


use Drupal\Core\Url;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\ConfirmFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\node\NodeInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

class ConfirmDeleteRangeDateForm extends ConfirmFormBase {
  protected $current_user;
  protected $entityTypeManager;
  protected $logger;
  
  protected $workcalendar;
  protected $rangedate;
  
  /**
   * Contruct implementation.
   * @param AccountProxyInterface $current_user
   * @param EntityTypeManagerInterface $entityTypeManager
   */
  public function __construct(AccountProxyInterface $current_user,
    EntityTypeManagerInterface $entityTypeManager, LoggerChannelFactoryInterface $logger) {
      $this->current_user = $current_user;
      $this->entityTypeManager = $entityTypeManager;
      $this->logger = $logger->get('efichajes - rangedate');
  }
  
  /**
   * Create implementation.
   * @param ContainerInterface $container
   * @return \Drupal\efichajes\Form\ConfirmDeleteSigningTypeForm
   */
  public static function create(ContainerInterface $container) {
    return new static (
      $container->get('current_user'),
      $container->get('entity_type.manager'),
      $container->get('logger.factory')
    );
  }
  
  /**
   *
   * {@inheritDoc}
   * @see \Drupal\Core\Form\FormInterface::getFormId()
   */
  public function getFormId() {
    return 'efichajesConfirmDeleteRangeDateForm';
  }
  
  public function validateForm(array &$form, FormStateInterface $form_state) {
    if ( !isset($this->workcalendar) || ($this->workcalendar->getType() != 'workcalendar') ) {
      $form_state->setErrorByName('WorkCalendar', $this->t('Error: No work calendar set or node type incorrect.'));
    }
    
    if ( !isset($this->rangedate) || ($this->rangedate->getType() != 'rangedate') ) {
      $form_state->setErrorByName('RangeDate', $this->t('Error: No range date set or node type incorrect.'));
    }
    
    if ( isset($this->rangedate) && ($this->rangedate->get('field_efichajes_id_workcalendar')->getValue()[0]['target_id'] != $this->workcalendar->id()) ) {
      $form_state->setErrorByName('RangeDate', $this->t('Error: Range date not owned by work calendar.'));
    }
  }
  
  /**
   *
   * {@inheritDoc}
   * @see \Drupal\Core\Form\ConfirmFormBase::buildForm()
   */
  public function buildForm(array $form, FormStateInterface $form_state, NodeInterface $node_wc = NULL, NodeInterface $node_rc = NULL) {
    $this->workcalendar = $node_wc;
    $this->rangedate = $node_rc;
    
    return parent::buildForm($form, $form_state);
  }
  
  /**
   *
   * {@inheritDoc}
   * @see \Drupal\Core\Form\ConfirmFormInterface::getQuestion()
   */
  public function getQuestion() {
    return $this->t('Are you sure you want to delete this Range Date?');
  }
  
  /**
   *
   * {@inheritDoc}
   * @see \Drupal\Core\Form\ConfirmFormBase::getConfirmText()
   */
  public function getConfirmText() {
    return $this->t('Delete');
  }
  
  /**
   *
   * {@inheritDoc}
   * @see \Drupal\Core\Form\ConfirmFormBase::getCancelText()
   */
  public function getCancelText() {
    return $this->t('Cancel');
  }
  
  /**
   *
   * {@inheritDoc}
   * @see \Drupal\Core\Form\ConfirmFormInterface::getCancelUrl()
   */
  public function getCancelUrl() {
    return new Url('efichajes.manageworkcalendar',[
      'operation' => 'alter',
      'node' => $this->workcalendar->id(),
    ]);
  }
  
  /**
   * 
   * {@inheritDoc}
   * @see \Drupal\Core\Form\FormInterface::submitForm()
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->rangedate->delete();
    drupal_set_message($this->t('Range date deleted.'));
    
    $this->logger->info('@login - rangedate - Range Date @nid deleted.', [
      '@login' => $this->current_user->getUsername(),
      '@nid' => $this->rangedate->id(),
    ]);
    
    $form_state->setRedirectUrl($this->getCancelUrl());
  }

}