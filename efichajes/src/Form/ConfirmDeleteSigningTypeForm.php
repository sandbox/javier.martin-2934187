<?php

namespace Drupal\efichajes\Form;

use Drupal\Core\Form\ConfirmFormBase;
use Drupal\Core\Session\AccountProxyInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\node\NodeInterface;
use Drupal\Core\Url;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;

class ConfirmDeleteSigningTypeForm extends ConfirmFormBase {
  protected $current_user;
  protected $entityTypeManager;
  protected $node;
  protected $logger;
  
  /**
   * Contruct implementation.
   * @param AccountProxyInterface $current_user
   * @param EntityTypeManagerInterface $entityTypeManager
   */
  public function __construct(AccountProxyInterface $current_user, 
    EntityTypeManagerInterface $entityTypeManager, LoggerChannelFactoryInterface $logger) {
    $this->current_user = $current_user;
    $this->entityTypeManager = $entityTypeManager;
    $this->logger = $logger->get('efichajes');
  }
  
  /**
   * Create implementation.
   * @param ContainerInterface $container
   * @return \Drupal\efichajes\Form\ConfirmDeleteSigningTypeForm
   */
  public static function create(ContainerInterface $container) {
    return new static (
      $container->get('current_user'),
      $container->get('entity_type.manager'),
      $container->get('logger.factory')
    );
  }
  
  /**
   * 
   * {@inheritDoc}
   * @see \Drupal\Core\Form\FormInterface::getFormId()
   */
  public function getFormId() {
    return 'efichajesCofirmDeleteSigningTypeForm';
  }

  /**
   * 
   * {@inheritDoc}
   * @see \Drupal\Core\Form\ConfirmFormBase::buildForm()
   */
  public function buildForm(array $form, FormStateInterface $form_state, NodeInterface $node = NULL) {
    $this->node = $node;
    
    return parent::buildForm($form, $form_state);
  }
  
  /**
   * 
   * {@inheritDoc}
   * @see \Drupal\Core\Form\ConfirmFormInterface::getQuestion()
   */
  public function getQuestion() {
    return $this->t('Are you sure you want to delete this Signing Type?');
  }
  
  /**
   * 
   * {@inheritDoc}
   * @see \Drupal\Core\Form\ConfirmFormBase::getConfirmText()
   */
  public function getConfirmText() {
    return $this->t('Delete');
  }
  
  /**
   * 
   * {@inheritDoc}
   * @see \Drupal\Core\Form\ConfirmFormBase::getCancelText()
   */
  public function getCancelText() {
    return $this->t('Cancel');
  }
  
  /**
   * 
   * {@inheritDoc}
   * @see \Drupal\Core\Form\ConfirmFormInterface::getCancelUrl()
   */
  public function getCancelUrl() {
    return new Url('efichajes.config.signingtypes');
  }
  
  /**
   * Return true if signing type is assigned to any signing.
   * @return boolean
   */
  protected function checkAssignedSigningType () {
    $storage = $this->entityTypeManager->getStorage('node');
    $nodes = $storage->loadByProperties([
      'type' => 'signing'
    ]);
    
    foreach ($nodes as $key => $value) {
      $signingtypeid = $value->get('field_efichajes_id_signing_type')->target_id;
      if ($this->node->id() == $signingtypeid) {
        return TRUE;
      }
    }
    
    return FALSE;
  }
  
  /**
   * Remove signing type if not assigned to any signing.
   * {@inheritDoc}
   * @see \Drupal\Core\Form\FormInterface::submitForm()
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    if ( isset($this->node) && ($this->node->getType() == 'signingtype') ) {
      if (!$this->checkAssignedSigningType()) {
        $this->node->delete();
        drupal_set_message($this->t('Signing type deleted'));
        
        $this->logger->info('@login - DeleteSigningType - Signing type @title deleted.', [
          '@login' => $this->current_user->getUsername(),
          '@title' => $this->node->getTitle(),
        ]);
      } else {
        $this->node->get('field_efichajes_enabled')->setValue(FALSE);
        $this->node->save();
      }
    } else {
      drupal_set_message($this->t('Signing type error.'), 'Error');
    }
    
    $form_state->setRedirectUrl($this->getCancelUrl());
  }
}