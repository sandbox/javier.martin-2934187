<?php 

namespace Drupal\efichajes\Form;

use Drupal\Core\Url;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\ConfirmFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\node\NodeInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

class ConfirmDeleteWorkCalendar extends ConfirmFormBase {
  protected $current_user;
  protected $entityTypeManager;
  protected $node;
  protected $logger;
  
  /**
   * Contruct implementation.
   * @param AccountProxyInterface $current_user
   * @param EntityTypeManagerInterface $entityTypeManager
   */
  public function __construct(AccountProxyInterface $current_user,
    EntityTypeManagerInterface $entityTypeManager, LoggerChannelFactoryInterface $logger) {
      $this->current_user = $current_user;
      $this->entityTypeManager = $entityTypeManager;
      $this->logger = $logger->get('efichajes - workcalendar');
  }
  
  /**
   * Create implementation.
   * @param ContainerInterface $container
   * @return \Drupal\efichajes\Form\ConfirmDeleteSigningTypeForm
   */
  public static function create(ContainerInterface $container) {
    return new static (
      $container->get('current_user'),
      $container->get('entity_type.manager'),
      $container->get('logger.factory')
      );
  }
  
  /**
   *
   * {@inheritDoc}
   * @see \Drupal\Core\Form\FormInterface::getFormId()
   */
  public function getFormId() {
    return 'efichajesCofirmDeleteWorkCalendar';
  }
  
  /**
   *
   * {@inheritDoc}
   * @see \Drupal\Core\Form\ConfirmFormBase::buildForm()
   */
  public function buildForm(array $form, FormStateInterface $form_state, NodeInterface $node = NULL) {
    $this->node = $node;
    
    return parent::buildForm($form, $form_state);
  }
  
  /**
   *
   * {@inheritDoc}
   * @see \Drupal\Core\Form\ConfirmFormInterface::getQuestion()
   */
  public function getQuestion() {
    return $this->t('Are you sure you want to delete this work calendar?');
  }
  
  /**
   *
   * {@inheritDoc}
   * @see \Drupal\Core\Form\ConfirmFormBase::getConfirmText()
   */
  public function getConfirmText() {
    return $this->t('Delete');
  }
  
  /**
   *
   * {@inheritDoc}
   * @see \Drupal\Core\Form\ConfirmFormBase::getCancelText()
   */
  public function getCancelText() {
    return $this->t('Cancel');
  }
  
  /**
   *
   * {@inheritDoc}
   * @see \Drupal\Core\Form\ConfirmFormInterface::getCancelUrl()
   */
  public function getCancelUrl() {
    return new Url('efichajes.config.workcalendars');
  }
  
  /**
   * 
   * {@inheritDoc}
   * @see \Drupal\Core\Form\FormBase::validateForm()
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    if (!isset($this->node) || $this->node->getType() != 'workcalendar') {
      $form_state->setErrorByName('Node Content', $this->t('Sorry, you can delete an empty node or node type distinct to workcalendar.'));
    }
  }
  
  /**
   * 
   * {@inheritDoc}
   * @see \Drupal\Core\Form\FormInterface::submitForm()
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // Check if work calendar is assigned to any user
    // Check if work calendar is assigned to any signing.
    $nid = $this->node->id();
    $this->node->delete();
    
    $this->logger->info('@login - WorkCalendar - Work calendar @nid deleted.', [
      '@login' => $this->current_user->getUsername(),
      '@nid' => $nid,
    ]);
    
    drupal_set_message($this->t('Work calendar deleted.'));
    $form_state->setRedirectUrl($this->getCancelUrl());
  }
}