<?php

namespace Drupal\efichajes\Form;

use Drupal\Core\Form\ConfirmFormBase;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\user\UserInterface;
use Drupal\Core\Url;

class ConfirmDeleteWorker extends ConfirmFormBase {
  protected $current_user;
  protected $entityTypeManager;
  protected $logger;
  protected $user;
  
  /**
   * Construct implementation.
   * @param AccountProxyInterface $current_user
   * @param EntityTypeManagerInterface $entityTypeManager
   * @param LoggerChannelFactoryInterface $logger
   */
  public function __construct(AccountProxyInterface $current_user, 
      EntityTypeManagerInterface $entityTypeManager, LoggerChannelFactoryInterface $logger) {
    $this->current_user = $current_user;
    $this->entityTypeManager = $entityTypeManager;
    $this->logger = $logger->get('efichajes');
  }
  
  /**
   * Create implementation.
   * @param ContainerInterface $container
   * @return \Drupal\efichajes\Form\ConfirmDeleteWorker
   */
  public static function create (ContainerInterface $container) {
    return new static (
      $container->get('current_user'),
      $container->get('entity_type.manager'),
      $container->get('logger.factory')
    );
  }
  
  /**
   * 
   * {@inheritDoc}
   * @see \Drupal\Core\Form\FormInterface::getFormId()
   */
  public function getFormId() {
    return 'efichajesConfirmDeleteWorkerForm';
  }
  
  /**
   * 
   * {@inheritDoc}
   * @see \Drupal\Core\Form\ConfirmFormBase::buildForm()
   */
  public function buildForm(array $form, FormStateInterface $form_state, UserInterface $user = null) {
    $this->user = $user;
    
    return parent::buildForm($form, $form_state);
  }
  
  /**
   * 
   * {@inheritDoc}
   * @see \Drupal\Core\Form\ConfirmFormInterface::getQuestion()
   */
  public function getQuestion() {
    return $this->t('Are you sure you want to delete this worker? (worker only be deleted if not have signing assigned)');
  }
  
  /**
   * 
   * {@inheritDoc}
   * @see \Drupal\Core\Form\ConfirmFormBase::getConfirmText()
   */
  public function getConfirmText() {
    return $this->t('Delete');
  }
  
  /**
   * 
   * {@inheritDoc}
   * @see \Drupal\Core\Form\ConfirmFormBase::getCancelText()
   */
  public function getCancelText() {
    return $this->t('Cancel');
  }
  
  /**
   * 
   * {@inheritDoc}
   * @see \Drupal\Core\Form\ConfirmFormInterface::getCancelUrl()
   */
  public function getCancelUrl() {
    return new Url('efichajes.config.workers');
  }
  
  /**
   * 
   * {@inheritDoc}
   * @see \Drupal\Core\Form\FormBase::validateForm()
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    if (!isset($this->user)) {
      $form_state->setErrorByName('user', $this->t('User not valid'));
    }
    
    if ( isset($user) && (!$this->user->hasPermission('efichajes worker')) ) {
      $form_state->setErrorByName('user', $this->t('This form is only valid to delete users with "efichajes worker" permission'));
    }
  }
  
  /**
   * 
   * {@inheritDoc}
   * @see \Drupal\Core\Form\FormInterface::submitForm()
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $storage = $this->entityTypeManager->getStorage('node');
    $signings = $storage->loadByProperties([
      'field_efichajes_user' => $this->user->id(),
    ]);
    
    // If user have signings -> disabled
    // else -> delete
    if (empty($signings)) {
      $this->user->delete();
      drupal_set_message($this->t('User @login deleted.', [
        '@login' => $this->user->getAccountName(),
      ]));
      
      $this->logger->info('@login - DeleteWorker - User @worker deleted.', [
        '@login' => $this->current_user->getUsername(),
        '@worker' => $this->user->getAccountName(),
      ]);
    } else {
      $this->user->get('status')->setValue(FALSE);
      $this->user->save();
      
      drupal_set_message($this->t('User @login disabled.', [
        '@login' => $this->user->getAccountName(),
      ]));
      
      $this->logger->info('@login - DeleteWorker - User @worker disabled.', [
        '@login' => $this->current_user->getUsername(),
        '@worker' => $this->user->getAccountName(),
      ]);
    }
    
    $form_state->setRedirectUrl($this->getCancelUrl());
  }
}