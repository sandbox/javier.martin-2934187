<?php

/**
 * @file
 * Add Workers form implementation.
 */

namespace Drupal\efichajes\Form;

use Drupal\Core\Link;
use Drupal\Core\Url;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\File\FileSystemInterface;
use Egulias\EmailValidator\EmailValidatorInterface;
use Drupal\file\FileUsage\DatabaseFileUsageBackend;
use Drupal\Core\Mail\MailManagerInterface;
use Drupal\Core\Language\LanguageManagerInterface;

class ImportWorkersForm extends FormBase {
  protected $current_user;
  protected $entityTypeManager;
  protected $fileSystem;
  protected $logger;
  protected $emailValidator;
  protected $fileUsage;
  protected $mailManager;
  protected $languageManager;
  
  /**
   * Contruct implementation.
   * 
   * @param AccountProxyInterface $current_user
   * @param EntityTypeManagerInterface $entityTypeManager
   * @param LoggerChannelFactoryInterface $logger
   * @param FileSystemInterface $fileSystem
   * @param EmailValidatorInterface $emailValidator
   * @param DatabaseFileUsageBackend $fileUsage
   * @param MailManagerInterface $mailManager
   * @param LanguageManagerInterface $languageManager
   */
  public function __construct(AccountProxyInterface $current_user, 
      EntityTypeManagerInterface $entityTypeManager, LoggerChannelFactoryInterface $logger,
      FileSystemInterface $fileSystem, EmailValidatorInterface $emailValidator,
      DatabaseFileUsageBackend $fileUsage, MailManagerInterface $mailManager,
      LanguageManagerInterface $languageManager) {
    $this->current_user = $current_user;
    $this->entityTypeManager = $entityTypeManager;
    $this->logger = $logger->get('efichajes');
    $this->fileSystem = $fileSystem;
    $this->emailValidator = $emailValidator;
    $this->fileUsage = $fileUsage;
    $this->mailManager = $mailManager;
    $this->languageManager = $languageManager;
  }
  
  /**
   * Create function implementation.
   * 
   * @param ContainerInterface $container
   * @return \Drupal\efichajes\Form\AddWorkersForm
   */
  public static function create(ContainerInterface $container) {
    return new static (
        $container->get('current_user'),
        $container->get('entity_type.manager'),
        $container->get('logger.factory'),
        $container->get('file_system'),
        $container->get('email.validator'),
        $container->get('file.usage'),
        $container->get('plugin.manager.mail'),
        $container->get('language_manager')
    );
  }
  
  /**
   * 
   * {@inheritDoc}
   * @see \Drupal\Core\Form\FormInterface::getFormId()
   */
  public function getFormId() {
    return 'efichajesAddWorkerForm';
  }
  
  /**
   * Return all efichajes import users files.
   * @return array
   */
  protected function getFiles() {
    $storage_file = $this->entityTypeManager->getStorage('file');
    $files = $storage_file->loadMultiple();
    $values = [];
    
    foreach ($files as $key => $value) {
      $fileusage = $this->fileUsage->listUsage($value);
      if ( array_key_exists('efichajes', $fileusage) ) {
        $uri = $value->get('uri')->value;
        $filename = $value->getFilename();
        
        // Generate url to download file and use it to generate url object.
        $uri_private = file_create_url($uri);
        
        $url = Url::fromUri($uri_private);
        $link = Link::fromTextAndUrl($filename, $url);
        
        $values[$value->id()]['filename'] = $link->toRenderable();
      }
    }

    return $values;
  }
  
  /**
   * Process a file and create users.
   * 
   * @param int $fid
   */
  protected function processFile(int $fid) {
    $user_storage = $this->entityTypeManager->getStorage('user');
    $file_storage = $this->entityTypeManager->getStorage('file');
    
    $file = $file_storage->load($fid);
    $uri = $file->get('uri')->value;
    $filename = $file->get('filename')->value;
    $path = $this->fileSystem->realpath($uri);
    
    $this->logger->info('@login - AddWorker - Init processing @file file.', [
      '@login' => $this->current_user->getUsername(),
      '@file' => $filename,
    ]);
    
    $lines = 0; $correct = 0; $error = 0; $exists = 0;
    
    // Open file and start to read
    $fp = fopen($path, "r");
    while (!feof($fp)) {
      $line = fgetcsv($fp, null, ';');
      
      // Count fields on line and check email.
      if ( (sizeof($line) != 4) || (!$this->emailValidator->isValid($line[3])) ) {
        $lines++; 
        $error++;
        continue;
      }
      
      // Check if user exists.
      $users = $user_storage->loadByProperties(['name' => $line[0]]);
      if (!empty($users)) {
        $lines++;
        $exists++;
        continue;
      }
      
      // Create user.
      $user_values = [
        'field_efichajes_user_id' => $line[0],
        'field_efichajes_user_name' => $line[1],
        'field_efichajes_user_surname' => $line[2],
        'roles' => ['efichajes_worker'],
        'mail' => $line[3],
        'name' => $line[0],
        'pass' => substr( md5(microtime()), 1, 8),
        'status' => 1
      ];
      $user = $user_storage->create($user_values);
      $user->save();
      
      // Notify account created.
      $module = 'efichajes';
      $key = 'create_account';
      $to = $user_values['mail'];
      $params = array();
      $params['name'] = $user_values['field_efichajes_user_name'];
      $params['surname'] = $user_values['field_efichajes_user_surname'];
      $params['login'] = $user_values['name'];
      $params['password'] = $user_values['pass'];
      $language_code = $this->languageManager->getDefaultLanguage()->getId();
      $send_now = TRUE;
      
      $this->mailManager->mail($module, $key, $to, $language_code, $params, NULL, $send_now);
      
      $this->logger->info('@login - AddWorker - User @id added.', [
        '@login' => $this->current_user->getUsername(),
        '@id' => $line[0],
      ]);
      
      $lines++;
      $correct++;
    }
    fclose($fp);
    $this->fileUsage->add($file, 'efichajes', 'user', $this->current_user->id(), 1);
    
    
    drupal_set_message($this->t('@file processed: @lines lines, @correct correct, @error errors, @duplicate duplicates', [
      '@file' => $filename,
      '@lines' => $lines,
      '@correct' => $correct,
      '@error' => $error,
      '@duplicate' => $exists,
    ]));
    
    $this->logger->info('@login - AddWorker - End processing @file file.', [
      '@login' => $this->current_user->getUsername(),
      '@file' => $filename,
    ]);
  }
  
  public function validateForm(array &$form, FormStateInterface $form_state) {
    $file_storage = $this->entityTypeManager->getStorage('file');
    $fids = $form_state->getValue('file');
    
    // Check file mime type. Only text files are valid.
    foreach ($fids as $fid) {
      $file = $file_storage->load($fid);
      $uri = $file->get('uri')->value;
      $path = $this->fileSystem->realpath($uri);
      $finfo = finfo_open(FILEINFO_MIME_TYPE);
      if (finfo_file($finfo, $path) != 'text/plain') {
        $form_state->setErrorByName($file->get('filename')->value, 
            $this->t('@file - Incorrect mime type.', ['@file' => $file->get('filename')->value]));
      }
    }
  }
  
  /**
   * 
   * {@inheritDoc}
   * @see \Drupal\Core\Form\FormInterface::buildForm()
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['form_description'] = [
      '#markup' => $this->t('Use this form to add workers.'),
    ];
    
    $form['file'] = [
      '#type' => 'managed_file',
      '#multiple' => TRUE,
      '#title' => $this->t('Worker File'),
      '#upload_location' => 'private://efichajes',
      '#upload_validators' => [
        'file_validate_extensions' => ['csv'],
      ],
      '#description' => $this->t('Upload a Worker file in .csv format.'),
      '#required' => TRUE,
      '#weight' => 1,
    ];
    
    $form['actions'] = [
      '#type' => 'actions',
      '#weight' => 2,
    ];
    
    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Upload'),
    ];
    
    $header = [
      'filename' => $this->t('Filename'),
    ];
    
    $form['files'] = [
      '#type' => 'table',
      '#header' => $header,
      '#empty' => $this->t('No files uploaded.'),
      '#caption' => $this->t('Files'),
      '#weight' => 3,
    ];
    
    foreach ($this->getFiles() as $key => $value) {
      $form['files'][$key] = $value; 
    }
    
    $form_state->disableCache();
    
    return $form;
  }
  
  /**
   * 
   * {@inheritDoc}
   * @see \Drupal\Core\Form\FormInterface::submitForm()
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    foreach ($form_state->getValue('file') as $fid) {
      $this->processFile($fid);
    }
    $form_state->setRedirect('efichajes.config.workers');
  }
}