<?php

namespace Drupal\efichajes\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\Password\PasswordInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Form\FormStateInterface;

class Login extends FormBase {
  protected $current_user;
  protected $entityTypeManager;
  protected $logger;
  protected $passwordHasher;
  
  /**
   * Implement construct.
   * @param AccountProxyInterface $current_user
   * @param EntityTypeManagerInterface $entityTypeManager
   * @param LoggerChannelFactoryInterface $logger
   * @param PasswordInterface $passwordHasher
   */
  public function __construct(AccountProxyInterface $current_user,
      EntityTypeManagerInterface $entityTypeManager, LoggerChannelFactoryInterface $logger,
      PasswordInterface $passwordHasher) {
    $this->current_user = $current_user;
    $this->entityTypeManager = $entityTypeManager;
    $this->logger = $logger->get('efichajes');
    $this->passwordHasher = $passwordHasher;
  }
  
  /**
   * 
   * {@inheritDoc}
   * @see \Drupal\Core\Form\FormBase::create()
   */
  public static function create (ContainerInterface $container) {
    return new static (
      $container->get('current_user'),
      $container->get('entity_type.manager'),
      $container->get('logger.factory'),
      $container->get('password')
    );
  }
  
  /**
   * 
   * {@inheritDoc}
   * @see \Drupal\Core\Form\FormInterface::getFormId()
   */
  public function getFormId() {
    return 'efichajesLoginForm';
  }
  
  /**
   * 
   * {@inheritDoc}
   * @see \Drupal\Core\Form\FormBase::validateForm()
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    $login = $form_state->getValue('username');
    $password = $form_state->getValue('password');
    
    $storage = $this->entityTypeManager->getStorage('user');
    $users = $storage->loadByProperties(['name' => $login]);
    $user = reset($users);
    
    if (!$this->current_user->isAnonymous()) {
      $form_state->setErrorByName('user', $this->t('You are already logged. Logout and back to this url to do login again.'));
    }
    
    if (empty($user)) {
      $form_state->setErrorByName('user', $this->t('@user not exists', ['@user' => $login]));
    } else {
      if (!$this->passwordHasher->check($password, $user->get('pass')->value)) {
        $form_state->setErrorByName('password', $this->t('Password incorrect'));
      }
      
      if ( (!$user->hasPermission('efichajes worker')) && (!$user->hasPermission('efichajes admin')) ) {
        $form_state->setErrorByName('permission', $this->t('Your user are not allowed to login.'));
      }
    }
    
  }
  
  /**
   * 
   * {@inheritDoc}
   * @see \Drupal\Core\Form\FormInterface::buildForm()
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['username'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Login'),
      '#description' => $this->t('Intro your login')
    ];
    
    $form['password'] = [
      '#type' => 'password',
      '#title' => $this->t('Password'),
      '#description' => $this->t('Intro password'),
    ];
    
    $form['actions'] = [
      '#type' => 'actions',
    ];
    
    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Submit'),
    ];
    
    return $form;
  }
  
  /**
   * 
   * {@inheritDoc}
   * @see \Drupal\Core\Form\FormInterface::submitForm()
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {    
    $login = $form_state->getValue('username');
    
    $storage = $this->entityTypeManager->getStorage('user');
    $users = $storage->loadByProperties(['name' => $login]);
    $user = reset($users);
    
    user_login_finalize($user);
    
    $this->logger->info('@login - AddSigning - Login successfull for @loginworker.', [
      '@login' => $this->current_user->getUsername(),
      '@loginworker' => $user->getUsername(),
    ]);
    
    $form_state->setRedirect('efichajes.signing', ['user' => $user->id()]);
  }
}