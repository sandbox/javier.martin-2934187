<?php 

namespace Drupal\efichajes\Form;

use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\Session\AccountProxyInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Drupal\Core\Form\FormStateInterface;
use Drupal\node\NodeInterface;

class ManageRangeDateForm extends FormBase {
  protected $current_user;
  protected $entityTypeManager;
  protected $logger;
  
  protected $operation;
  protected $workcalendar;
  protected $rangedate;
  
  /**
   * Construct implementation.
   * @param AccountProxyInterface $current_user
   * @param EntityTypeManagerInterface $entityTypeManager
   * @param LoggerChannelFactoryInterface $logger
   */
  public function __construct(AccountProxyInterface $current_user,
    EntityTypeManagerInterface $entityTypeManager, LoggerChannelFactoryInterface $logger) {
      $this->current_user = $current_user;
      $this->entityTypeManager = $entityTypeManager;
      $this->logger = $logger->get('efichajes - rangedate');
  }
  
  /**
   * Create implementation.
   * @param ContainerInterface $container
   * @return \Drupal\efichajes\Form\WorkersForm
   */
  public static function create(ContainerInterface $container) {
    return new static (
      $container->get('current_user'),
      $container->get('entity_type.manager'),
      $container->get('logger.factory')
    );
  }
  
  /**
   *
   * {@inheritDoc}
   * @see \Drupal\Core\Form\FormInterface::getFormId()
   */
  public function getFormId() {
    return 'efichajesManageRangeDateForm';
  }
  
  public function validateForm(array &$form, FormStateInterface $form_state) {
    $storage = $this->entityTypeManager->getStorage('node');
    $rangedates = $storage->loadByProperties([
      'type' => 'rangedate',
      'field_efichajes_id_workcalendar' => $this->workcalendar->id(),
    ]);
    
    $start_date = \Datetime::createFromFormat('Y-m-d', $form_state->getValue('start_date')); 
    $end_date = \Datetime::createFromFormat('Y-m-d', $form_state->getValue('end_date')); 
    
    // Check start date less than end date
    if ($start_date->getTimestamp() > $end_date->getTimestamp()) {
      $form_state->setErrorByName('StartEndDates', $this->t('Error: Start date always must be less than or equal to end date.'));
    }
    
    // If work period, check if checked work week days
    if ($form_state->getValue('workday')) {
      $flag = FALSE;
      foreach ($form_state->getValue('workweekdays') as $key => $value) {
        if ($key == $value) {
            $flag = TRUE;
            break;
        }
      }
      
      if (!$flag) {
        $form_state->setErrorByName('WorkWeekDays', $this->t('Error: You have checked this range date as work period but not checked work days.'));
      }
    }
    
    // Check if exist collision with other range date for this work calendar.
    $start_date->setTime(0, 0);
    $end_date->setTime(23, 59);
    foreach ($rangedates as $value) {
      if ( !isset($this->rangedate) || ($this->rangedate->id() != $value->id()) ) {
        $range_start_date = $value->get('field_efichajes_start_date')->value;
        $range_end_date = $value->get('field_efichajes_end_date')->value;
        if (
          ( ( $start_date->getTimestamp() >= $range_start_date) && ( $end_date->getTimestamp() <= $range_end_date ) ) ||
          ( ( $end_date->getTimestamp() >= $range_start_date ) && ( $end_date->getTimestamp() <= $range_end_date ) )
          ) 
        {
          $form_state->setErrorByName('RangeDate', $this->t('Error: Found Range date collission @nid. Check start and end dates.', [
            '@nid' => $value->id(),
          ]));
        }
      }
    }
    
  }
  
  /**
   * 
   * {@inheritDoc}
   * @see \Drupal\Core\Form\FormInterface::buildForm()
   */
  public function buildForm(array $form, FormStateInterface $form_state, 
    NodeInterface $node_wc = null, string $operation = 'add', NodeInterface $node_rc = null) {
    // Check arguments.
      if ( 
      ( ($operation == 'add') && ( !isset($node_wc) || ($node_wc->getType() != 'workcalendar')) ) ||
      ( ($operation == 'alter') && ( !isset($node_wc) || ($node_wc->getType() != 'workcalendar') || !isset($node_rc) || ($node_rc->getType() != 'rangedate')) ) ||
        (!in_array($operation, ['add', 'alter']))
      ) {
        drupal_set_message($this->t('You have been redirected: Error on arguments types.'));
        global $base_url;
        $response = new RedirectResponse($base_url . '/admin/config/efichajes/workcalendars');
        $response->send();
        exit;
    }
    
    $this->operation = $operation;
    $this->workcalendar = $node_wc;
    $this->rangedate = $node_rc;
    
    $form['form_description'] = [
      '#markup' => $this->t('Use this form to create a new range date and assign it to work calendar @workcalendar', [
        '@workcalendar' => $node_wc->getTitle()]),
    ];
    
    $default_start_date = new \DateTime(); 
    $default_end_date = new \DateTime(); 
    $default_worktime_date = new \DateTime();
    if ( isset($this->rangedate) ) {
      $default_start_date->setTimestamp($this->rangedate->get('field_efichajes_start_date')->value);
      $default_end_date->setTimestamp($this->rangedate->get('field_efichajes_end_date')->value);
      $default_worktime_date->setTimestamp($this->rangedate->get('field_efichajes_worktime')->value);
      
    } else {
      $default_start_date->setTime(7, 0);
      $default_end_date->setTime(7, 0);
      $default_worktime_date->setTime(7, 0);
    }
    
    $form['start_date'] = [
      '#type' => 'date',
      '#title' => $this->t('Start Date'),
      '#description' => $this->t('Intro start date for range date.'),
      '#required' => TRUE,
      '#default_value' => $default_start_date->format('Y-m-d'),
    ];
    
    $form['end_date'] = [
      '#type' => 'date',
      '#title' => $this->t('End Date'),
      '#description' => $this->t('Intro end date for range date.'),
      '#required' => TRUE,
      '#default_value' => $default_end_date->format('Y-m-d'),
    ];
    
    $form['workday'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('WorkDay'),
      '#description' => $this->t('Check if this range date is laborable period.'),
      '#default_value' => isset($this->rangedate) ?
        $this->rangedate->get('field_efichajes_workday')->value : FALSE, 
    ];
    
    $form['workweekdays'] = [
      '#type' => 'checkboxes',
      '#title' => $this->t('Laborable Days per Week'),
      '#description' => $this->t('Check only laborables days.'),
      '#options' => [
        'monday' => $this->t('Monday'),
        'tuesday' => $this->t('Tuesday'),
        'wednesday' => $this->t('Wednesday'),
        'thursday' => $this->t('Thursday'),
        'friday' => $this->t('Friday'),
        'saturday' => $this->t('Saturday'),
        'sunday' => $this->t('Sunday'),
      ],
      '#states' => [
        'visible' => [
          ':input[name="workday"]' => ['checked' => TRUE],
        ],
      ],
      '#default_value' => ( isset($this->rangedate) )? 
        explode(',', $this->rangedate->get('field_efichajes_weekworkdays')->value) : ['monday', 'tuesday', 'wednesday', 'thursday', 'friday'],
    ];
    
    $form['worktime'] = [
      '#type' => 'datetime',
      '#title' => $this->t('Daily Work Time per day.'),
      '#description' => $this->t('Intro work time per day in hours and minutes.'),
      '#date_date_element' => 'none',
      '#date_time_element' => 'time',
      '#date_time_format' => 'H:i',
      '#default_value' => DrupalDateTime::createFromDateTime($default_worktime_date),
    ];
    
    $form['actions'] = [
      '#type' => 'actions',
    ];
    
    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Submit'),
    ];
    
    return $form;
  }
  
  /**
   * 
   * {@inheritDoc}
   * @see \Drupal\Core\Form\FormInterface::submitForm()
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // Set field values
    $title = $this->workcalendar->id() . ' - ' . $this->workcalendar->getTitle() . ' | ' .
      $form_state->getValue('start_date') . ' - ' . $form_state->getValue('end_date');
    $start_date = \DateTime::createFromFormat('Y-m-d', $form_state->getValue('start_date')); $start_date->setTime(0,0);
    $end_date = \DateTime::createFromFormat('Y-m-d', $form_state->getValue('end_date')); $end_date->setTime(23, 59);
    $work_day = $form_state->getValue('workday');
    $work_week_days = [];
    foreach ($form_state->getValue('workweekdays') as $key => $value) {
      if (is_string($value)) $work_week_days[] = $key;
    }
    $work_time = $form_state->getValue('worktime')->getTimestamp();
    
    // Add or alter entity rangedate
    if ($this->operation == 'add') {
      $storage = $this->entityTypeManager->getStorage('node');
      $rangedate = $storage->create([
        'type' => 'rangedate',
        'title' => $title,
        'field_efichajes_start_date' => $start_date->getTimestamp(),
        'field_efichajes_end_date' => $end_date->getTimestamp(),
        'field_efichajes_workday' => $work_day,
        'field_efichajes_weekworkdays' => implode(',', $work_week_days),
        'field_efichajes_id_workcalendar' => $this->workcalendar->id(),
        'field_efichajes_worktime' => $work_time,
      ]);
      $rangedate->save();
      
      $this->logger->info('@login - RangeDate - New Range Date created with nid @nid_rd belong to work calendar @nid_wc.', [
        '@login' => $this->current_user->getUsername(),
        '@nid_wc' => $this->workcalendar->id(),
        '@nid_rc' => $rangedate->id(),
      ]);
    } else if ($this->operation == 'alter') {
      $this->rangedate->setTitle($title);
      $this->rangedate->set('field_efichajes_start_date', $start_date->getTimestamp());
      $this->rangedate->set('field_efichajes_end_date', $end_date->getTimestamp());
      $this->rangedate->set('field_efichajes_workday', $work_day);
      $this->rangedate->set('field_efichajes_weekworkdays', implode(',', $work_week_days));
      $this->rangedate->set('field_efichajes_id_workcalendar', $this->workcalendar->id());
      $this->rangedate->set('field_efichajes_worktime', $work_time);
      $this->rangedate->save();
      
      $this->logger->info('@login - RangeDate - Range Date update with nid @nid_rd belong to work calendar @nid_wc.', [
        '@login' => $this->current_user->getUsername(),
        '@nid_wc' => $this->workcalendar->id(),
        '@nid_rd' => $this->rangedate->id(),
      ]);
    }
    
    $form_state->setRedirect('efichajes.manageworkcalendar', [
      'operation' => 'alter',
      'node' => $this->workcalendar->id(),
    ]);
  }
}