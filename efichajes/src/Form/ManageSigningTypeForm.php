<?php

/**
 * @file
 * Signing type modal form.
 */

namespace Drupal\efichajes\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Drupal\node\NodeInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;

class ManageSigningTypeForm extends FormBase {
  
  protected $current_user;
  protected $entityTypeManager;
  protected $logger;
  protected $node;
  protected $operation;
  
  /**
   * Construct implementation.
   * @param AccountProxyInterface $current_user
   * @param EntityTypeManagerInterface $entityTypeManager
   */
  public function __construct(AccountProxyInterface $current_user, 
      EntityTypeManagerInterface $entityTypeManager, LoggerChannelFactoryInterface $logger) {
    $this->current_user = $current_user;
    $this->entityTypeManager = $entityTypeManager;
    $this->logger = $logger->get('efichajes');
  }
  
  /**
   * Create implementation.
   * @param ContainerInterface $container
   * @return \Drupal\efichajes\Form\ManageSigningTypeForm
   */
  public static function create(ContainerInterface $container) {
    return new static (
      $container->get('current_user'),
      $container->get('entity_type.manager'),
      $container->get('logger.factory')
    );
  }
  
  /**
   * 
   * {@inheritDoc}
   * @see \Drupal\Core\Form\FormInterface::getFormId()
   */
  public function getFormId() {
    return 'efichajesManageSigningTypeForm';
  }
  
  /**
   * 
   * {@inheritDoc}
   * @see \Drupal\Core\Form\FormInterface::buildForm()
   */
  public function buildForm(array $form, FormStateInterface $form_state, string $operation = 'add', NodeInterface $node = NULL) {
    // Get operation value and store on formstate.
    $this->node = $node;
    $this->operation = $operation;
    
    if (!is_string($operation)) {
      throw new BadRequestHttpException($this->t('Error on arguments types.'));
    }
    
    $form['form_description'] = [
      '#markup' => $this->t('Use this form to @operation a signing type.', 
        array('@operation' => $operation)),
    ];
    
    if (!isset($this->node) && ($operation == 'alter') ) {
      drupal_set_message($this->t('Signing type not exists.'), 'error');
    }
    
    $form['description'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Description'),
      '#default_value' => isset($this->node) ? $this->node->getTitle() : '',
      '#size' => 50,
      '#maxlength' => 50,
      '#required' => TRUE,
      '#disabled' => !isset($this->node) && ($operation == 'alter') ? TRUE : FALSE,
    ];
    
    $form['enabled'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enabled'),
      '#default_value' => isset($this->node) ? $this->node->get('field_efichajes_enabled')->getValue()[0]['value'] : FALSE,
      '#disabled' => !isset($this->node) && ($operation == 'alter') ? TRUE : FALSE,
    ];
    
    $form['actions'] = [
      '#type' => 'actions',
    ];
    
    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Send'),
      '#disabled' => !isset($this->node) && ($operation == 'alter') ? TRUE : FALSE,
    ];
    
    return $form;
  }
  
  /**
   * 
   * {@inheritDoc}
   * @see \Drupal\Core\Form\FormInterface::submitForm()
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    if ($this->operation == 'add') {
      $storage = $this->entityTypeManager->getStorage('node');
      $signingtype = $storage->create(['type' => 'signingtype',
        'title' => $form_state->getValue('description'),
        'field_efichajes_enabled' => $form_state->getValue('enabled'),
      ]);
      $signingtype->save();
      
      drupal_set_message($this->t('New signing type @title created.',[
        '@title' => $form_state->getValue('description'),
      ]));
      
      $this->logger->info('@login - AddSigningType - Signing type @title created.', [
        '@login' => $this->current_user->getUsername(),
        '@title' => $form_state->getValue('description'),
      ]);
      
    } else if ($this->operation == 'alter'){
      $this->node->setTitle($form_state->getValue('description'));
      $this->node->get('field_efichajes_enabled')->setValue($form_state->getValue('enabled'));
      $this->node->save();
      
      drupal_set_message($this->t('Updated signing type @title.', [
        '@title' => $form_state->getValue('description'),
      ]));
      
      $this->logger->info('@login - AlterSigningType - Signing type @title updated.', [
        '@login' => $this->current_user->getUsername(),
        '@title' => $form_state->getValue('description'),
      ]);
    }
    
    $form_state->setRedirect('efichajes.config.signingtypes');
  }
}