<?php

/**
 * @file
 * Implement manage work calendar.
 */

namespace Drupal\efichajes\Form;

use Drupal\Core\Link;
use Drupal\Core\Url;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\Session\AccountProxyInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\node\NodeInterface;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;

class ManageWorkCalendarForm extends FormBase {
  protected $current_user;
  protected $entityTypeManager;
  protected $logger;
  
  protected $operation;
  protected $workcalendar;
  
  /**
   * Construct implementation.
   * @param AccountProxyInterface $current_user
   * @param EntityTypeManagerInterface $entityTypeManager
   * @param LoggerChannelFactoryInterface $logger
   */
  public function __construct(AccountProxyInterface $current_user,
    EntityTypeManagerInterface $entityTypeManager, LoggerChannelFactoryInterface $logger) {
      $this->current_user = $current_user;
      $this->entityTypeManager = $entityTypeManager;
      $this->logger = $logger->get('efichajes - workcalendar');
  }
  
  /**
   * Create implementation.
   * @param ContainerInterface $container
   * @return \Drupal\efichajes\Form\WorkersForm
   */
  public static function create(ContainerInterface $container) {
    return new static (
      $container->get('current_user'),
      $container->get('entity_type.manager'),
      $container->get('logger.factory')
    );
  }
  
  /**
   *
   * {@inheritDoc}
   * @see \Drupal\Core\Form\FormInterface::getFormId()
   */
  public function getFormId() {
    return 'efichajesManageWorkCalendarForm';
  }
  
  protected function getRangeDates() {
    $query = \Drupal::entityQuery('node')
      ->condition('type', 'rangedate')
      ->condition('field_efichajes_id_workcalendar', $this->workcalendar->id())
      ->sort('field_efichajes_start_date', 'DESC');
    $ids = $query->execute();
    $storage = $this->entityTypeManager->getStorage('node');
    $rangedates = $storage->loadMultiple($ids);
    $results = [];
    foreach ($rangedates as $key => $value) {
      $results[$key]['id'] = [
        '#markup' => $value->id(),
      ];
     
      $temp_date = new \DateTime(); 
      
      $temp_date->setTimestamp($value->get('field_efichajes_start_date')->value);
      $results[$key]['start_date'] = [
        '#markup' => $temp_date->format('d/m/Y'),
      ];
      
      $temp_date->setTimestamp($value->get('field_efichajes_end_date')->value);
      $results[$key]['end_date'] = [
        '#markup' => $temp_date->format('d/m/Y'),
      ];
      
      $results[$key]['workperiod'] = [
        '#markup' => isset($value->get('field_efichajes_worktime')->value) ? $this->t('Enabled') : $this->t('Disabled'),
      ];
      
      $url_alter = Url::fromRoute('efichajes.managerangedate', [
        'node_wc' => $this->workcalendar->id(),
        'operation' => 'alter',
        'node_rd' => $value->id(),
      ]);
      
      $url_delete = Url::fromRoute('efichajes.managerangedate', [
        'node_wc' => $this->workcalendar->id(),
        'operation' => 'delete',
        'node_rd' => $value->id(),
      ]);
      
      $link_alter = Link::fromTextAndUrl($this->t('Alter'), $url_alter);
      $link_delete = Link::fromTextAndUrl($this->t('Delete'), $url_delete);
      
      $results[$key]['alter'] = $link_alter->toRenderable();
      $results[$key]['delete'] = $link_delete->toRenderable();
    }
    
    return $results;
  }
  
  /**
   * 
   * {@inheritDoc}
   * @see \Drupal\Core\Form\FormInterface::buildForm()
   */
  public function buildForm(array $form, FormStateInterface $form_state, string $operation = 'add', NodeInterface $node = null) {
    // Check arguments
    if ( in_array($operation, ['alter', 'delete'])  && ( !isset($node) || ( $node->getType() != 'workcalendar') ) ) 
    {
      drupal_set_message($this->t('You have been redirected: Error on arguments types.'));
      global $base_url;
      $response = new RedirectResponse($base_url . '/admin/config/efichajes/workcalendars');
      $response->send();
      exit;
    }
    
    $this->operation = $operation;
    $this->workcalendar = $node;
    
    $form['form_description'] = [
      '#markup' => $this->t('Use this form to add work calendars.'),
      '#weight' => 1,
    ];
    
    $form['description'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Work calendar name'),
      '#description' => $this->t('Intro work calendar name.'),
      '#required' => TRUE,
      '#default_value' => isset($this->workcalendar) ? $this->workcalendar->getTitle() : '',
      '#weight' => 2,
    ];
    
    $form['enable'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enable'),
      '#description' => $this->t('Check to enable work calendar.'),
      '#default_value' => isset($this->workcalendar) ? $this->workcalendar->get('field_efichajes_enabled')->value : TRUE,
      '#weight' => 3,
    ];
    
    $form['actions'] = [
      '#type' => 'actions',
      '#weight' => 4,
    ];
    
    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Submit'),
    ];
    
    $headers = [
      $this->t('Id'),
      $this->t('Start date'),
      $this->t('End date'),
      $this->t('Work Period'),
      $this->t('Alter'),
      $this->t('Delete'),
    ];
    
    $form['rangedates_table'] = [
      '#type' => 'table',
      '#caption' => $this->t('Result Dates'),
      '#header' => $headers,
      '#empty' => $this->t('No range dates found.'),
      '#weight' => 5,
    ];
    
    foreach ($this->getRangeDates() as $key => $value) {
      $form['rangedates_table'][$key] = $value;
    }
    
    return $form;
  }
  
  /**
   * 
   * {@inheritDoc}
   * @see \Drupal\Core\Form\FormInterface::submitForm()
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    if ($this->operation == 'add') {
      $storage = $this->entityTypeManager->getStorage('node');
      $values = [
        'type' => 'workcalendar',
        'title' => $form_state->getValue('description'),
        'field_efichajes_enabled' => $form_state->getValue('enabled'),
      ];
      $new_workcalendar = $storage->create($values);
      $new_workcalendar->save();
      
      $this->logger->info('@login - WorkCalendar - New work calendar created with nid @nid.', [
        '@login' => $this->current_user->getUsername(),
        '@nid' => $new_workcalendar->id(),
      ]);
      
      drupal_set_message($this->t('Work calendar created with nid @nid', [
        '@nid' => $new_workcalendar->id(),
      ]));
    } else if ( ($this->operation == 'alter') && 
      (isset($this->workcalendar)) && 
      ($this->workcalendar->getType() == 'workcalendar' ) ) {
      $this->workcalendar->setTitle($form_state->getValue('description'));
      $this->workcalendar->get('field_efichajes_enabled')->setValue($form_state->getValue('enable'));
      $this->workcalendar->save();
      
      $this->logger->info('@login - WorkCalendar - New work calendar created with nid @nid.', [
        '@login' => $this->current_user->getUsername(),
        '@nid' => $this->workcalendar->id(),
      ]);
      
      drupal_set_message($this->t('Work calendar @description altered.', [
        '@description' => $this->workcalendar->getTitle(),
      ]));
    }
    
    $form_state->setRedirect('efichajes.config.workcalendars');
  }
  
  
}