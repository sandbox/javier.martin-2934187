<?php

namespace Drupal\efichajes\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Language\LanguageManagerInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\Mail\MailManagerInterface;
use Egulias\EmailValidator\EmailValidatorInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\user\UserInterface;

class ManageWorkerForm extends FormBase {
  protected $current_user;
  protected $entityTypeManager;
  protected $logger;
  protected $emailValidator;
  protected $mailManager;
  protected $languageManager;
  
  protected $operation;
  protected $user;
  
  /**
   * Construct implementation.
   * @param AccountProxyInterface $current_user
   * @param EntityTypeManagerInterface $entityTypeManager
   * @param LoggerChannelFactoryInterface $logger
   * @param EmailValidatorInterface $emailValidator
   */
  public function __construct(AccountProxyInterface $current_user,
      EntityTypeManagerInterface $entityTypeManager, LoggerChannelFactoryInterface $logger,
    EmailValidatorInterface $emailValidator, MailManagerInterface $mailManager,
    LanguageManagerInterface $languageManager) {
    $this->current_user = $current_user;
    $this->entityTypeManager = $entityTypeManager;
    $this->logger = $logger->get('efichajes');
    $this->emailValidator = $emailValidator;
    $this->mailManager = $mailManager;
    $this->languageManager = $languageManager;
  }
  
  /**
   * Create implementation.
   * @param ContainerInterface $container
   * @return \Drupal\efichajes\Form\ManageWorkerForm
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('current_user'),
      $container->get('entity_type.manager'),
      $container->get('logger.factory'),
      $container->get('email.validator'),
      $container->get('plugin.manager.mail'),
      $container->get('language_manager'));
  }
  
  /**
   * 
   * {@inheritDoc}
   * @see \Drupal\Core\Form\FormInterface::getFormId()
   */
  public function getFormId() {
    return 'efichajesManageWorkerForm';
  }
  
  /**
   * Validate login, id and email.
   * {@inheritDoc}
   * @see \Drupal\Core\Form\FormBase::validateForm()
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    $storage = $this->entityTypeManager->getStorage('user');
    $login = $form_state->getValue('login');
    $id = $form_state->getValue('id');
    $email = $form_state->getValue('email');
    
    $users = $storage->loadByProperties(['name' => $login]);
    
    if ($this->operation == 'add') {
      if (!empty($users)) {
        $form_state->setErrorByName('login', $this->t('User @login exist.', ['@login' => $login]));
      }
      
      $users = $storage->loadByProperties(['field_efichajes_user_id' => $id]);
      if (!empty($users)) {
        $form_state->setErrorByName('id', $this->t('User with @id exist.', ['@id' => $id]));
      }
      
      if (!$this->emailValidator->isValid($email)) {
        $form_state->setErrorByName('email', $this->t('Email @email not valid.', ['@email' => $email]));
      }
    } else if ($this->operation == 'alter') {
      if (!isset($this->user)) {
        $form_state->setErrorByName('user', $this->t('User not valid'));
      }
      
      if (isset($this->user) && (!$this->user->hasPermission('efichajes worker')) ) {
        $form_state->setErrorByName('user', $this->t('User not valid. This form is only for user with "efichajes worker" permission.'));
      }
      
      // We are changing login, so we have to check if new login exists.
      if ( ($login != $this->user->get('name')->value) && (!empty($users)) ) {
        $form_state->setErrorByName('login', $this->t('User @login exist.', ['@login' => $login]));
      }
      
      // We are changing user id, so we have to check if new id exists.
      $users = $storage->loadByProperties(['field_efichajes_user_id' => $id]);
      if ( ($id != $this->user->get('field_efichajes_user_id')->value) && (!empty($users)) ) {
        $form_state->setErrorByName('id', $this->t('User with id @id exist.', ['@id' => $id]));
      }
      
      if (!$this->emailValidator->isValid($email)) {
        $form_state->setErrorByName('email', $this->t('Email @email not valid.', ['@email' => $email]));
      }
    }
    
  }
  
  /**
   * 
   * {@inheritDoc}
   * @see \Drupal\Core\Form\FormInterface::buildForm()
   */
  public function buildForm(array $form, FormStateInterface $form_state, 
      string $operation = null, UserInterface $user = null) {
    $this->operation = $operation;
    $this->user = $user;
        
    $form['form_description'] = [
      '#markup' => $this->t('Use this form to @operation a user.', [
        '@operation' => $operation,
      ]),
    ];
    
    $form['login'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Login'),
      '#description' => $this->t('User login'),
      '#default_value' => isset($user) ? $user->get('name')->value : '',
      '#required' => TRUE,
    ];
    
    $form['email'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Email'),
      '#description' => $this->t('User email'),
      '#default_value' => isset($user) ? $user->get('mail')->value : '',
      '#required' => TRUE,
    ];
    
    $form['id'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Id'),
      '#description' => $this->t('User id'),
      '#default_value' => isset($user) ? $user->get('field_efichajes_user_id')->value : '',
      '#required' => TRUE,
    ];
    
    $form['name'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Name'),
      '#description' => $this->t('User Name'),
      '#default_value' => isset($user) ? $user->get('field_efichajes_user_name')->value : '',
      '#required' => TRUE,
    ];
    
    $form['surname'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Surname'),
      '#description' => $this->t('User surname'),
      '#default_value' => isset($user) ? $user->get('field_efichajes_user_surname')->value : '',
      '#required' => TRUE,
    ];
    
    $form['password'] = [
      '#type' => 'password_confirm',
      '#title' => $this->t('Password'),
      '#description' => $this->t('Keep this field empty to maintain password.'),
    ];
    
    $form['actions'] = [
      '#type' => 'actions',
    ];
    
    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Submit'),
    ];
    
    return $form;
  }
  
  /**
   * 
   * {@inheritDoc}
   * @see \Drupal\Core\Form\FormInterface::submitForm()
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $storage = $this->entityTypeManager->getStorage('user');
    
    if ($this->operation == 'add') {
      $user = $storage->create([
        'field_efichajes_user_id' => $form_state->getValue('id'),
        'field_efichajes_user_name' => $form_state->getValue('name'),
        'field_efichajes_user_surname' => $form_state->getValue('surname'),
        'roles' => ['efichajes_worker'],
        'mail' => $form_state->getValue('email'),
        'name' => $form_state->getValue('login'),
        'pass' => $form_state->getValue('password'),
        'status' => 1
      ]);
      $user->save();
      
      // Notify account created.
      $module = 'efichajes';
      $key = 'create_account';
      $to = $user->get('mail')->value;
      $params = array();
      $params['name'] = $user->get('field_efichajes_user_name')->value;
      $params['surname'] = $user->get('field_efichajes_user_surname')->value;
      $params['login'] = $user->get('name')->value;
      $params['password'] = $user->get('pass')->value;
      $language_code = $this->languageManager->getDefaultLanguage()->getId();
      $send_now = TRUE;
      
      $this->mailManager->mail($module, $key, $to, $language_code, $params, NULL, $send_now);
      
      drupal_set_message(
        $this->t('User @login created.', [
          '@login' => $form_state->getValue('login')
        ]));
      
      $this->logger->info('@login - AddWorker - User @logincreated created.', [
        '@login' => $this->current_user->getUsername(),
        '@logincreated' => $form_state->getValue('login'),
      ]);
    } else if ($this->operation == 'alter') {
      $user = $storage->load($this->user->id());
      
      $user->get('name')->setValue($form_state->getValue('login'));
      $user->get('mail')->setValue($form_state->getValue('email'));
      $user->get('field_efichajes_user_id')->setValue($form_state->getValue('id'));
      $user->get('field_efichajes_user_name')->setValue($form_state->getValue('name'));
      $user->get('field_efichajes_user_surname')->setValue($form_state->getValue('surname'));
      
      if (!empty($form_state->getValue('password'))) {
        $user->get('pass')->setValue($form_state->getValue('password'));
      }
      
      $user->save();
      
      // Notify account altered.
      $module = 'efichajes';
      $key = 'alter_account';
      $to = $user->get('mail')->value;
      $params = array();
      $params['name'] = $user->get('field_efichajes_user_name')->value;
      $params['surname'] = $user->get('field_efichajes_user_surname')->value;
      $params['login'] = $user->get('name')->value;
      $language_code = $this->languageManager->getDefaultLanguage()->getId();
      $send_now = TRUE;
      
      $this->mailManager->mail($module, $key, $to, $language_code, $params, NULL, $send_now);
      
      drupal_set_message(
          $this->t('User @login updated.', [
            '@login' => $form_state->getValue('login')
          ]));
      
      $this->logger->info('@login - AlterWorker - User @logincreated updated.', [
        '@login' => $this->current_user->getUsername(),
        '@loginalter' => $form_state->getValue('login'),
      ]);
    }
    
    $form_state->setRedirect('efichajes.config.workers');
  }
  
}