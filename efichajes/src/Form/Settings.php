<?php 

namespace Drupal\efichajes\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

class Settings extends ConfigFormBase {
  
  /**
   * 
   * {@inheritDoc}
   * @see \Drupal\Core\Form\FormInterface::getFormId()
   */
  public function getFormId() {
    return 'efichajesSettingsForm';
  }
  
  /**
   * 
   * @return string[]
   */
  protected function getEditableConfigNames() {
    return [
      'efichajes.settings',
    ];
  }
  
  /**
   * 
   * {@inheritDoc}
   * @see \Drupal\Core\Form\FormBase::validateForm()
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    
    if ($form_state->getValue('check_ip_filter')) {
      $validip_pattern = "/^(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)$/";
      $array_ips = preg_split("/[\s,]+/", $form_state->getValue('ip_list'));
      
      foreach ($array_ips as $value) {
        if (!preg_match($validip_pattern, $value)) {
          $form_state->setErrorByName('Ip Value', $this->t('@ip is not a valid ip value', [
            '@ip' => $value,
          ]));
        }
      }
    }
    
    return parent::validateForm($form, $form_state);
  }
  
  /**
   * 
   * {@inheritDoc}
   * @see \Drupal\Core\Form\ConfigFormBase::buildForm()
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('efichajes.settings');
    
    $form['ipgroup'] = [
      '#type' => 'details',
      '#title' => $this->t('Ip Filter'),
      '#description' => $this->t('Enable/Disable ip filter for signings.'),
      '#open' => TRUE, 
    ];
    
    $form['ipgroup']['check_ip_filter'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Ip filter'),
      '#description' => $this->t('Check if you want to allow signing only from determined ips.'),
      '#default_value' => $config->get('allowed_ip_filter'),
    ];
    
    $form['ipgroup']['ip_list'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Ip list'),
      '#description' => $this->t('Intro one ip per line.'),
      '#default_value' => $config->get('ip_list'),
      '#cols' => 20,
      '#rows' => 5,
      '#states' => [
        'visible' => [
          ':input[name="check_ip_filter"]' => ['checked' => TRUE],
        ],
      ],
    ];
    
    $form['mails_body'] = [
      '#type' => 'details',
      '#title' => $this->t('Email Notifications'),
      '#description' => $this->t('Fill all email bodys.'),
      '#open' => TRUE,
    ];
    
    $form['mails_body']['create_account'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Create account mail body'),
      '#description' => $this->t('This mail will be sent when user account is created. Valid tokens for this email are @name, @surname, @login and @password.'),
      '#default_value' => $config->get('mail_create_account'),
      '#cols' => 60,
      '#rows' => 10,
    ];
    
    $form['mails_body']['alter_account'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Alter account mail body'),
      '#description' => $this->t('This mail will be sent when user account is altered. Valid tokens for this email are @name and @surname.'),
      '#default_value' => $config->get('mail_alter_account'),
      '#cols' => 60,
      '#rows' => 10,
    ];
    
    return parent::buildForm($form, $form_state);
  }
  
  /**
   * 
   * {@inheritDoc}
   * @see \Drupal\Core\Form\ConfigFormBase::submitForm()
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    
    $this->config('efichajes.settings')
      ->set('allowed_ip_filter', $form_state->getValue('check_ip_filter'))
      ->set('ip_list', $form_state->getValue('ip_list'))
      ->set('mail_create_account', $form_state->getValue('create_account'))
      ->set('mail_alter_account', $form_state->getValue('alter_account'))
      ->save();
    
    return parent::submitForm($form, $form_state);
  }
  
}