<?php

namespace Drupal\efichajes\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\user\UserInterface;
use Symfony\Component\HttpFoundation\RequestStack;

class Signing extends FormBase {
  protected $current_user;
  protected $entityTypeManager;
  protected $logger;
  protected $requestStack;
  protected $user;
  
  /**
   * Construct implement.
   * @param AccountProxyInterface $current_user
   * @param EntityTypeManagerInterface $entityTypeManager
   * @param LoggerChannelFactoryInterface $logger
   */
  public function __construct(AccountProxyInterface $current_user,
      EntityTypeManagerInterface $entityTypeManager,
      LoggerChannelFactoryInterface $logger,
      RequestStack $requestStack) {
    $this->current_user = $current_user;
    $this->entityTypeManager = $entityTypeManager;
    $this->logger = $logger->get('efichajes');
    $this->requestStack = $requestStack;
  }
  
  /**
   * Create implementation.
   * @param ContainerInterface $container
   * @return \Drupal\efichajes\Form\Signing
   */
  public static function create(ContainerInterface $container) {
    return new static (
      $container->get('current_user'),
      $container->get('entity_type.manager'),
      $container->get('logger.factory'),
      $container->get('request_stack')
    );
  }
  
  /**
   * 
   * {@inheritDoc}
   * @see \Drupal\Core\Form\FormInterface::getFormId()
   */
  public function getFormId() {
    return 'efichajessigningform';
  }
  
  /**
   * Returns all enabled signings types.
   * @return array
   */
  protected function getSigningTypes() {
    $values = [];
    $storage = $this->entityTypeManager->getStorage('node');
    $signings = $storage->loadByProperties([
      'type' => 'signingtype',
      'field_efichajes_enabled' => TRUE,
    ]);
    
    
    foreach ($signings as $key => $value) {
      $values[$key] = $value->get('title')->getValue()[0]['value'];
    }
    
    return $values;
  }
  
  /**
   * Return all signings for actual user for today.
   * @return array
   */
  protected function getSignings() {
    $values = [];
    $storage = $this->entityTypeManager->getStorage('node');
    $signings = $storage->loadByProperties([
      'type' => 'signing',
      'field_efichajes_user' => $this->user->id(),
    ]);
    
    $date_ini = \DateTime::createFromFormat('d/m/Y H:i:s', date('d/m/Y') . ' 00:00:00', new \DateTimeZone('UTC'));
    $date_fin = \DateTime::createFromFormat('d/m/Y H:i:s', date('d/m/Y') . ' 23:59:59', new \DateTimeZone('UTC'));
    
    $query = $storage->getQuery()
    ->condition('type', 'signing')
    ->condition('status', 1)
    ->condition('field_efichajes_user', $this->user->id())
    ->condition('field_efichajes_date', array($date_ini->getTimestamp(), $date_fin->getTimestamp()), 'between')
    ->condition('field_efichajes_enabled', TRUE);

    $signings = $query->execute();
    
    foreach ($signings as $value) {
      $signing = $storage->load($value);
      $signing_date = $signing->get('field_efichajes_date')->getValue()[0]['value'];
      $signing_type = $storage->load($signing->get('field_efichajes_id_signing_type')->getValue()[0]['target_id']);
      
      $values[] = [
        'date' => date('d/m/Y H:i:s', $signing_date),
        'signingtype' => $signing_type->getTitle(),
      ];
    }

    return $values;
  }
  
  /**
   * 
   * {@inheritDoc}
   * @see \Drupal\Core\Form\FormBase::validateForm()
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    // Check if user can send signing
    if ( 
        ($this->current_user->id() != $this->user->id()) && 
        (!$this->current_user->hasPermission('efichajes admin'))
    ) {
      $form_state->setErrorByName('User', $this->t('Your user has no permissions to send this signing.'));
      dpm('Error');
    }
  }
  
  /**
   * 
   * {@inheritDoc}
   * @see \Drupal\Core\Form\FormInterface::buildForm()
   */
  public function buildForm(array $form, FormStateInterface $form_state, UserInterface $user = null) {
    $this->user = $user;
    
    $form['#attached']['library'][] = 'efichajes/timeclock';
    
    $form['form_description'] = [
      '#markup' => $this->t('Use this form to add a signing.'),
    ];
    
    $form['name'] = [
      '#markup' => isset($user) ? 
        $user->get('field_efichajes_user_surname')->getValue()[0]['value'] . ', ' .
        $user->get('field_efichajes_user_name')->getValue()[0]['value'] : 
        $this->t('User not found.'),
      '#prefix' => '<div id="name">',
      '#suffix' => '</div>',
    ];
    
    $form['date'] = [
      '#markup' => date('d/m/Y'),
      '#prefix' => '<div id="date">',
      '#suffix' => '</div>',
    ];
    
    $form['hour'] = [
      '#markup' => date('H:i:s'),
      '#prefix' => '<div id="hour">',
      '#suffix' => '</div>',
    ];
    
    $form['signingtype'] = [
      '#type' => 'select',
      '#title' => $this->t('Signing types'),
      '#description' => $this->t('Select signing type'),
      '#options' => $this->getSigningTypes(),
    ];
    
    $form['actions'] = [
      '#type' => 'actions',
    ];
    
    $form['submit'] = [
      '#type' => 'submit',
      '#value' => 'Submit',
    ];
    
    $header = [
      'date' => $this->t('Date'),
      'signingtype' => $this->t('Signing type'),
    ];
    
    $form['daysignings'] = [
      '#type' => 'table',
      '#header' => $header,
      '#rows' => isset($user) ? $this->getSignings() : array(),
      '#empty' => $this->t('No signings for today.'),
    ];
    
    return $form;
  }
  
  /**
   * 
   * {@inheritDoc}
   * @see \Drupal\Core\Form\FormInterface::submitForm()
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    if (isset($this->user)) {
      $storage = $this->entityTypeManager->getStorage('node');
      $request = $this->requestStack->getCurrentRequest();
      $server = $request->server;
      $parameters = $server->all();
      $ip_client = $parameters['HTTP_X_FORWARDED_FOR'];
      $browser_info = $parameters['HTTP_USER_AGENT'];
      $signing_type = $storage->load($form_state->getValue('signingtype'));
      
      $signing_title = 'Signing | ' . 
        $this->user->id() . ' - ' . $this->user->getUsername() . ' | ' .
        $signing_type->id() . ' - ' . $signing_type->getTitle() . ' | ' .
        date('d/m/Y H:i:s', time());
      
      $values = [
        'type' => 'signing',
        'title' => $signing_title,
        'field_efichajes_data' => $ip_client . ' ' . $browser_info,
        'field_efichajes_date' => time(),
        'field_efichajes_enabled' => TRUE,
        'field_efichajes_id_signing_type' => $form_state->getValue('signingtype'),
        'field_efichajes_user' => $this->user->id(),        
      ];
      $signing = $storage->create($values);
      $signing->save();
      
      drupal_set_message($this->t('Signing inserted.'));
      $this->logger->info('@login - AddSigning - Added signing for @loginworker.', [
        '@login' => $this->current_user->getUsername(),
        '@loginworker' => $this->user->getUsername(),
      ]);
    }
    
    $form_state->setRedirect('efichajes.signing', ['user' => $this->user->id()]);
  }
}