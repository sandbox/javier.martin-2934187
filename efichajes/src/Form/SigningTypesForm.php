<?php

/**
 * @file
 * Signing type form implementation.
 */

namespace Drupal\efichajes\Form;

use Drupal\Core\Url;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Session\AccountProxyInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Link;

class SigningTypesForm extends FormBase {
  
  protected $current_user;
  protected $entityTypeManager;
  
  /**
   * Construct implementation.
   * @param AccountProxyInterface $current_user
   * @param EntityTypeManagerInterface $entityTypeManager
   */
  public function __construct(AccountProxyInterface $current_user, EntityTypeManagerInterface $entityTypeManager) {
    $this->current_user = $current_user;
    $this->entityTypeManager = $entityTypeManager;
  }
  
  /**
   * Create implementation.
   * @param ContainerInterface $container
   * @return \Drupal\efichajes\Form\SigningTypesForm
   */
  public static function create(ContainerInterface $container) {
    return new static (
        $container->get('current_user'),
        $container->get('entity_type.manager')
    );
  }
  
  /**
   * 
   * {@inheritDoc}
   * @see \Drupal\Core\Form\FormInterface::getFormId()
   */
  public function getFormId() {
    return 'efichajesSigningTypesForm';
  }
  
  /**
   * Returns array with all signing types.
   * @return \Drupal\Core\StringTranslation\TranslatableMarkup[][]|NULL[][]|\Drupal\Core\Link[][]|\Drupal\Core\Entity\EntityInterface[][]
   */
  protected function getSignings() {
    $storage = $this->entityTypeManager->getStorage('node');
    $result = $storage->loadByProperties([
      'type' => 'signingtype',
    ]);
    
    $options = [];
    foreach ($result as $nid => $signingtype) {
      
      $url_alter = Url::fromRoute('efichajes.managesigningtype', [
        'operation' => 'alter',
        'node' => $nid,
      ]);
      
      $url_delete = Url::fromRoute('efichajes.managesigningtype', [
        'operation' => 'delete',
        'node' => $nid
      ]);
      
      $link_alter = Link::fromTextAndUrl($this->t('Alter'), $url_alter);
      $link_delete = Link::fromTextAndUrl($this->t('Delete'), $url_delete);
      
      $enabled = $signingtype->get('field_efichajes_enabled')->value;
      
      $options[$nid] = [
        'nid' => $nid,
        'description' => $signingtype->getTitle(),
        'enabled' => $enabled ? $this->t('Enabled') : $this->t('Disabled'),
        'alter' => $link_alter,
        'delete' => $link_delete,
      ];
    }
    
    return $options;
  }
  
  /**
   * 
   * {@inheritDoc}
   * @see \Drupal\Core\Form\FormInterface::buildForm()
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['#attached']['library'][] = 'core/drupal.dialog.ajax';
    
    $form['form_description'] = [
      '#markup' => $this->t('Use this form to manage signings types.'),  
    ];
    
    $header = [
      'nid' => $this->t('Signign Type Id'),
      'description' => $this->t('Description'),
      'enabled' => $this->t('Enabled'),
      'alter' => $this->t('Alter'),
      'delete' => $this->t('Delete'),
    ];
    
    $form['table'] = [
      '#type' => 'table',
      '#header' => $header,
      '#rows' => $this->getSignings(),
      '#empty' => $this->t('No signing types found.'),
    ];
    
    return $form;
  }
  
  /**
   * 
   * {@inheritDoc}
   * @see \Drupal\Core\Form\FormInterface::submitForm()
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    
  }
}