<?php

namespace Drupal\efichajes\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\user\UserInterface;
use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\ReplaceCommand;

class SigningsList extends FormBase {
  protected $current_user;
  protected $entityTypeManager;
  protected $logger;
  protected $user;
  
  /**
   * Construct implementation
   * @param AccountProxyInterface $current_user
   * @param EntityTypeManagerInterface $entityTypeManager
   * @param LoggerChannelFactoryInterface $logger
   */
  public function __construct(AccountProxyInterface $current_user,
      EntityTypeManagerInterface $entityTypeManager, LoggerChannelFactoryInterface $logger) {
    $this->current_user = $current_user;
    $this->entityTypeManager = $entityTypeManager;
    $this->logger = $logger->get('efichajes');
  }
  
  /**
   * Create implementation.
   * @param ContainerInterface $container
   * @return \Drupal\efichajes\Form\SigningsList
   */
  public static function create(ContainerInterface $container) {
    return new static (
      $container->get('current_user'),
      $container->get('entity_type.manager'),
      $container->get('logger.factory')
    );
  }
  
  /**
   * 
   * {@inheritDoc}
   * @see \Drupal\Core\Form\FormInterface::getFormId()
   */
  public function getFormId() {
    return 'efichajesSigningsListForm';
  }
  
  /**
   * Returns time worked on a single day.
   * @param array $signings
   * @return number
   */
  protected function getTime(array $signings) {
    $storage = $this->entityTypeManager->getStorage('node');
    $keys = array_keys($signings);
    $time = 0;
        
    $i = 0;
    while ($i < count($keys)) {
      if ( ($i+1) < count($keys) ) {       
        $signing_first = $storage->load($signings[$keys[$i]]);
        $signing_second = $storage->load($signings[$keys[$i+1]]);
        
        $date1 = $signing_first->get('field_efichajes_date')->value;
        $date2 = $signing_second->get('field_efichajes_date')->value;
        
        $time = $time + ($date2 - $date1);  
      }
      $i = $i + 2;
    }
    
    return $time;
  }
  
  /**
   * Returns rows to show on signings table.
   * @param \DateTime $date_ini
   * @param \DateTime $date_fin
   * @return string[]
   */
  protected function getSignings(\DateTime $date_ini, \DateTime $date_fin) {
    $rows = [];
    $totaltime = 0;
    $storage = $this->entityTypeManager->getStorage('node');

    // Init Dates
    $date_ini->setTime(0, 0); 
    $date_fin->setTime(0, 0);
    $temp = $date_ini; $temp->setTime(0, 0);
    
    while ($date_ini <= $date_fin) {
      $key = $date_ini->format('d/m/Y');
      
      $start_interval = \DateTime::createFromFormat('d/m/Y', $date_ini->format('d/m/Y'));
      $start_interval->setTime(0, 0);
      
      $end_interval = \DateTime::createFromFormat('d/m/Y', $date_ini->format('d/m/Y'));
      $end_interval->setTime(23, 59);
      
      $query = $storage->getQuery()
      ->condition('type', 'signing')
      ->condition('status', 1)
      ->condition('field_efichajes_user', $this->user->id())
      ->condition('field_efichajes_date', array(
        $start_interval->getTimestamp(),
        $end_interval->getTimestamp()
      ), 'between')
      ->condition('field_efichajes_enabled', TRUE)
      ->sort('field_efichajes_date', 'ASC');
      
      $signings = $query->execute();
      
      $rows[$key]['date'] = [
        '#markup' => $temp->format('d/m/Y'),
      ];
      
      if (!empty($signings)) {
        foreach ($signings as $value) {
          $signing = $storage->load($value);
          $signing_date = $signing->get('field_efichajes_date')->value;
          $signing_date_dt = new \DateTime(); $signing_date_dt->setTimestamp($signing_date);
          $signing_type = $storage->load($signing->get('field_efichajes_id_signing_type')->target_id);
          
          $rows[$key]['signings'][] = [
            '#prefix' => '<p>',
            '#suffix' => '</p>',
            '#markup' => $signing_date_dt->format('H:i:s') . ' - ' . $signing_type->getTitle(),
          ];
        }
      } else {
        $rows[$key]['signings'][] = [
          '#prefix' => '<p>',
          '#suffix' => '</p>',
          '#markup' => $this->t('No signings for today.'),
        ];
      }
      
      $temptime = $this->getTime($signings);
      $totaltime += $temptime;
      $rows[$key]['worktime'] = [
        '#markup' => gmdate('H:i:s', $temptime),
      ];
      
      $date_ini->add(new \DateInterval('P1D'));
    }
    
    $rows['summary']['date'][] = [
      '#markup' => '',
    ];
    
    $rows['summary']['signings'] = [
      '#prefix' => '<p><strong>',
      '#markup' => $this->t('Total'),
      '#suffix' => '</strong></p>',
    ];
    
    $rows['summary']['worktime'] = [
      '#markup' => gmdate('H:i:s', $totaltime),
    ];
    
    return $rows;
  }
  
  /**
   * Ajax function to refresh signings table.
   * @param array $form
   * @param FormStateInterface $form_state
   * @return \Drupal\Core\Ajax\AjaxResponse
   */
  public function ajaxrefreshsigningstable(array &$form, FormStateInterface $form_state) {
    $response = new AjaxResponse();
    $response->addCommand(new ReplaceCommand('#signings-table', render($form['signings'])));
    
    return $response;
  }
  
  /**
   * 
   * {@inheritDoc}
   * @see \Drupal\Core\Form\FormInterface::buildForm()
   */
  public function buildForm(array $form, FormStateInterface $form_state, UserInterface $user = null) {
    $form['#attached']['library'][] = 'efichajes/js-xlsx';
    $form['#attached']['library'][] = 'efichajes/jsPDF';
    $form['#attached']['library'][] = 'efichajes/signings-download';
    
    $this->user = $user;
    
    $element = $form_state->getTriggeringElement();
    if (empty($element)) {
      $date_ini = new \DateTime();
      $date_fin = new \DateTime();
    } else {
      $date_ini = new \DateTime($form_state->getValue('date_ini'));
      $date_fin = new \DateTime($form_state->getValue('date_fin'));
    }
    
    $form['form_description'] = [
      '#prefix' => '<p>',
      '#suffix' => '</p>',
      '#markup' => $this->t('Use this form to see your signings'),
      '#weight' => 1,
    ];
    
    $form['date_ini'] = [
      '#type' => 'datetime',
      '#name' => 'date_ini',
      '#title' => $this->t('Start date'),
      '#description' => $this->t('Select init date.'),
      '#date_date_element' => 'date',
      '#date_time_element' => 'none',
      '#default_value' => DrupalDateTime::createFromDateTime($date_ini),
      '#weight' => 2,
    ];
    
    $form['date_fin'] = [
      '#type' => 'datetime',
      '#name' => 'date_fin',
      '#title' => $this->t('End date'),
      '#description' => $this->t('Select end date.'),
      '#date_date_element' => 'date',
      '#date_time_element' => 'none',
      '#default_value' => DrupalDateTime::createFromDateTime($date_fin),
      '#weight' => 3,
    ];
    
    $form['actions'] = [
      '#type' => 'actions',
      '#weight' => 4,
    ];
    
    $form['actions']['submit'] = [
      '#type' => 'button',
      '#value' => $this->t('Submit'),
      '#name' => 'refreshbutton',
      '#ajax' => [
        'callback' => [$this, 'ajaxrefreshsigningstable'],
      ],
    ];
    
    $headers = [
      $this->t('Date'),
      $this->t('Signings'),
      $this->t('WorkTime'),
    ];
    
    $form['signings'] = [
      '#type' => 'table',
      '#caption' => $this->t('Signings'),
      '#header' => $headers,
      '#empty' => $this->t('No signings founds.'),
      '#attributes' => [
        'id' => 'signings-table',
      ],
      '#weight' => 5,
    ];
    
    $signings = $this->getSignings($date_ini, $date_fin);
    foreach ($signings as $key => $value) {
      $form['signings'][$key] = $value;
    }
    
    $form['actions-download'] = [
      '#type' => 'actions',
      '#weight' => 6,
    ];
    
    $form['actions-download']['button-xslx'] = [
      '#type' => 'button',
      '#value' => 'Download as xslx',
      '#name' => 'button-xslx',
      '#attributes' => [
        'id' => 'button-xlsx',
        'onclick' => 'return false;',
      ],
    ];
    
    $form['actions-download']['button-pdf'] = [
      '#type' => 'button',
      '#value' => 'Download as PDF',
      '#name' => 'button-pdf',
      '#attributes' => [
        'id' => 'button-pdf',
      ],
    ];
    
    return $form;
  }
  
  /**
   * 
   * {@inheritDoc}
   * @see \Drupal\Core\Form\FormInterface::submitForm()
   */
  public function submitForm(array &$form, FormStateInterface $form_state) { }
  
}