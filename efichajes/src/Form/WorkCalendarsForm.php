<?php 

namespace Drupal\efichajes\Form;

use Drupal\Core\Link;
use Drupal\Core\Url;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Session\AccountProxyInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Form\FormStateInterface;

class WorkCalendarsForm extends FormBase {
  protected $current_user;
  protected $entityTypeManager;
  
  /**
   * Construct implementation.
   * @param AccountProxyInterface $current_user
   * @param EntityTypeManagerInterface $entityTypeManager
   */
  public function __construct(AccountProxyInterface $current_user, EntityTypeManagerInterface $entityTypeManager) {
    $this->current_user = $current_user;
    $this->entityTypeManager = $entityTypeManager;
  }
  
  /**
   * Create implementation.
   * @param ContainerInterface $container
   * @return \Drupal\efichajes\Form\SigningTypesForm
   */
  public static function create(ContainerInterface $container) {
    return new static (
      $container->get('current_user'),
      $container->get('entity_type.manager')
      );
  }
  
  /**
   *
   * {@inheritDoc}
   * @see \Drupal\Core\Form\FormInterface::getFormId()
   */
  public function getFormId() {
    return 'manageWorkCarlendarsForm';
  }
  
  /**
   * Return array with all work calendars.
   */
  public function getWorkCalendars() {
    $storage = $this->entityTypeManager->getStorage('node');
    $results = $storage->loadByProperties(['type' => 'workcalendar']);
    
    $options = [];
    foreach ($results as $nid => $value) {
      $url_alter = Url::fromRoute('efichajes.manageworkcalendar', [
        'operation' => 'alter',
        'node' => $nid,
      ]);
      
      $url_delete = Url::fromRoute('efichajes.manageworkcalendar', [
        'operation' => 'delete',
        'node' => $nid
      ]);
      
      $link_alter = Link::fromTextAndUrl($this->t('Alter'), $url_alter);
      $link_delete = Link::fromTextAndUrl($this->t('Delete'), $url_delete);
      
      $options[$nid] = [
        'nid' => $value->id(),
        'description' => $value->getTitle(),
        'enabled' => $value->get('field_efichajes_enabled')->value ? $this->t('Enable') : $this->t('Disable'),
        'alter' => $link_alter,
        'delete' => $link_delete,
      ];
    }
    
    return $options;
  }
  
  /**
   * 
   * {@inheritDoc}
   * @see \Drupal\Core\Form\FormInterface::buildForm()
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['form_description'] = [
      '#markup' => $this->t('Use this form to manage work calendars.'),
    ];
    
    $header = [
      'nid' => $this->t('Work Calendar Id'),
      'description' => $this->t('Description'),
      'enabled' => $this->t('Enabled'),
      'alter' => $this->t('Alter'),
      'delete' => $this->t('Delete'),
    ];
    
    $form['table'] = [
      '#type' => 'table',
      '#header' => $header,
      '#rows' => $this->getWorkCalendars(),
      '#empty' => $this->t('No signing types found.'),
    ];
    
    return $form;
  }
  
  public function submitForm(array &$form, FormStateInterface $form_state) {
    
  }
}