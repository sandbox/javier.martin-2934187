<?php

/**
 * @file
 * Worker form implementation.
 */

namespace Drupal\efichajes\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Session\AccountProxyInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Link;
use Drupal\Core\Url;
use Drupal\Core\Entity\EntityTypeManagerInterface;

class WorkersForm extends FormBase {
  protected $current_user;
  protected $entityTypeManager;
  protected $logger;
  
  /**
   * Construct implementation.
   * @param AccountProxyInterface $current_user
   * @param EntityTypeManagerInterface $entityTypeManager
   * @param LoggerChannelFactoryInterface $logger
   */
  public function __construct(AccountProxyInterface $current_user, 
      EntityTypeManagerInterface $entityTypeManager, LoggerChannelFactoryInterface $logger) {
    $this->current_user = $current_user;
    $this->entityTypeManager = $entityTypeManager;
    $this->logger = $logger->get('efichajes - workersform');
  }
  
  /**
   * Create implementation.
   * @param ContainerInterface $container
   * @return \Drupal\efichajes\Form\WorkersForm
   */
  public static function create(ContainerInterface $container) {
    return new static (
        $container->get('current_user'),
        $container->get('entity_type.manager'),
        $container->get('logger.factory')
    );
  }
  
  /**
   * 
   * {@inheritDoc}
   * @see \Drupal\Core\Form\FormInterface::getFormId()
   */
  public function getFormId() {
    return 'efichajesWorkersForm';
  }
  
  /**
   * Return array with all users with efichajes_worker role assigned.
   * @return array
   */
  protected function getWorkers() {
    $user_storage = $this->entityTypeManager->getStorage('user');
    $users = $user_storage->loadByProperties([
      'roles' => ['efichajes_worker'],
    ]);
    
    $workers = array();
    foreach ($users as $user) {
      $key = $user->id();
      
      $url_alter = Url::fromRoute('efichajes.manageworkerform', [
        'operation' => 'alter',
        'user' => $user->id(),
      ]);
      
      $url_delete = Url::fromRoute('efichajes.manageworkerform', [
        'operation' => 'delete',
        'user' => $user->id(),
      ]);
      
      $url_signings = Url::fromRoute('efichajes.signingslist', [
        'user' => $user->id(),
      ]);
      
      $link_alter = Link::fromTextAndUrl($this->t('Alter'), $url_alter);
      $link_delete = Link::fromTextAndUrl($this->t('Delete'), $url_delete);
      $link_signings = Link::fromTextAndUrl($this->t('Signings'), $url_signings);
      
      $workers[$key] = [
        'id' => [
          '#markup' => $user->get('field_efichajes_user_id')->value,
        ],
        'surname' => [
          '#markup' => $user->get('field_efichajes_user_surname')->value,
        ],
        'name' => [
          '#markup' => $user->get('field_efichajes_user_name')->value,
        ],
        'alter' => $link_alter->toRenderable(),
        'delete' => $link_delete->toRenderable(),
        'signings' => $link_signings->toRenderable(),
      ];
    }
    
    return $workers;   
  }
  
  /**
   * 
   * {@inheritDoc}
   * @see \Drupal\Core\Form\FormInterface::buildForm()
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['form_description'] = [
      '#markup' => $this->t('Use this form to manage workers.'),
    ];
    
    $header = [
      'id' => $this->t('Id'),
      'surname' => $this->t('Surname'),
      'name' => $this->t('Name'),
      'alter' => $this->t('Alter'),
      'delete' => $this->t('Delete'),
      'signings' => $this->t('Signings'),
    ];
    
    $form['workers'] = [
      '#type' => 'table',
      '#header' => $header,
      '#empty' => $this->t('No workers found.'),
      '#caption' => $this->t('Workers'),
    ];
    
    foreach ($this->getWorkers() as $key => $value) {
      $form['workers'][$key] = $value;
    }
    
    return $form;
  }
  
  /**
   * 
   * {@inheritDoc}
   * @see \Drupal\Core\Form\FormInterface::submitForm()
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    
  }
  
}