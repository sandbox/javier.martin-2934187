<?php

namespace Drupal\efichajes\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Url;
use Drupal\Core\Session\AccountInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Link;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;

/**
 * Provides Block with worker links.
 * @author fjmlillo
 * 
 * @Block(
 *  id = "efichajesWorkerBlock",
 *  admin_label = @Translation("eFichajes - Worker Block") 
 * )
 *
 */
class WorkerBlock extends BlockBase implements ContainerFactoryPluginInterface {
  protected $current_user;
  
  /**
   * Implement construct function.
   * @param array $configuration
   * @param string $plugin_id
   * @param string $plugin_definition
   * @param AccountInterface $current_user
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, 
    AccountInterface $current_user) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);  
    $this->current_user = $current_user;
  }
  
  /**
   * Implement create static function.
   * @param ContainerInterface $container
   * @param array $configuration
   * @param string $plugin_id
   * @param string $plugin_definition
   * @return \Drupal\efichajes\Plugin\Block\WorkerBlock
   */
  public static function create (ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static (
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('current_user')
    );
  }
  
  /**
   * 
   * {@inheritDoc}
   * @see \Drupal\Core\Block\BlockPluginInterface::build()
   */
  public function build() {
    $form['description'] = [
      '#prefix' => '<p>',
      '#suffix' => '</p>',
      '#markup' => $this->t('In this block you can find links to do basic operation with your account.'),
    ];
    
    if ($this->current_user->hasPermission('efichajes worker')) {
      $id = $this->current_user->id();
      
      $url_signing = Url::fromRoute('efichajes.signing', ['user' => $id]);
      $url_signinglist = Url::fromRoute('efichajes.signingslist', ['user' => $id]);
      $url_changepassword = Url::fromRoute('efichajes.changepassword', ['user' => $id]);
      
      $link_signing = Link::fromTextAndUrl($this->t('Signing'), $url_signing);
      $link_signinglist = Link::fromTextAndUrl($this->t('My Signings List'), $url_signinglist);
      $link_changepassword = Link::fromTextAndUrl('Change Password', $url_changepassword);
      
      $list = [];
      $list[] = $link_signing->toRenderable();
      $list[] = $link_signinglist->toRenderable();
      $list[] = $link_changepassword->toRenderable();
      
      $form['list'] = [
        '#theme' => 'item_list',
        '#items' => $list,
        '#cache' => [
          'max-age' => 0,
        ],
      ];
    } else {
      $form['nouser'] = [
        '#markup' => $this->t('Sorry, you are not allowed to see this links. Contact with admin.'),
      ];
    }
    
    return $form;
  }
  
}