<?php 

namespace Drupal\efichajes\Plugin\Menu;

use Drupal\Core\Menu\LocalActionDefault;
use Drupal\Core\Routing\RouteMatchInterface;

class AddRangeDateAction extends LocalActionDefault {
  public function getRouteParameters(RouteMatchInterface $route_match) {
    $node = $route_match->getParameter('node');
    return [
      'node_wc' => $node->id(),
      'operation' => 'add',
    ];
  }
}