!function($) {
  $(document).ready(function() {
    // Timeclock
    (function() {
      setInterval(
        function () {
          var now = new Date();
          
          var hours = now.getHours();
          var minutes = now.getMinutes();
          var seconds = now.getSeconds();
          
          if (hours   < 10) {hours   = "0"+hours;}
          if (minutes < 10) {minutes = "0"+minutes;}
          if (seconds < 10) {seconds = "0"+seconds;}
          
          $('#timeclock').html(hours+':'+minutes+':'+seconds);
        },
        1000);
    })();
  });
}(jQuery);