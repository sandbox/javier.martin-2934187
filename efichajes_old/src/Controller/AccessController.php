<?php

namespace Drupal\efichajes\Controller;

use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Access\AccessResult;

class AccessController {
  /**
   * Access if user have efichajes module admin permission.
   * 
   * @param AccountInterface $account
   * @return \Drupal\Core\Access\AccessResult
   */
  public function accessModuleAdmin(AccountInterface $account) {
    return AccessResult::allowedIfHasPermission($account, 'efichajes module admin');
  }
  
  /**
   * Access if user have efichajes module admin or efichajes enterprise admin 
   * permission.
   * 
   * @param AccountInterface $account
   * @return boolean
   */
  public function accessModuleEnterpriseAdmin(AccountInterface $account){
    return AccessResult::allowedIfHasPermissions($account, [
      'efichajes module admin',
      'efichajes enterprise admin'], 'OR');
  }
  
  /**
   * Access if user have efichajes worker or efichajes enterprise admin
   * @param AccountInterface $account
   * @return \Drupal\Core\Access\AccessResult
   */
  public function accessWorkerEnterpriseAdmin(AccountInterface $account) {
    return AccessResult::allowedIfHasPermissions($account, [
        'efichajes worker',
        'efichajes enterprise admin'], 'OR');
  }
}