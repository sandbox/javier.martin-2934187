<?php

namespace Drupal\efichajes;

use Drupal\Core\Database\Connection;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Session\AccountProxy;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;

class EfichajesDatabase implements EfichajesDatabaseInterface {
  use StringTranslationTrait;
  
  protected $entityTypeManager;
  protected $database;
  protected $logger;
  protected $current_user;
  
  public function __construct(EntityTypeManagerInterface $entityTypeManager,
    Connection $database, LoggerChannelFactoryInterface $logger,
    AccountProxy $current_user) {
      $this->entityTypeManager = $entityTypeManager;
      $this->database = $database;
      $this->logger = $logger->get('eFichajes - databaseController');
      $this->current_user = $current_user;
  }
  
  public static function create(ContainerInterface $container) {
    return new static (
      $container->get('entity_type.manager'),
      $container->get('database'),
      $container->get('logger.factory'),
      $container->get('current_user')
    );
  }
  
  /**
   * Return user data by username
   * @param string $username
   * @return \Drupal\Core\Entity\EntityInterface[]
   */
  public function getUserByUsername($username) {
    $user_storage = $this->entityTypeManager->getStorage('user');
    $users = $user_storage->loadByProperties([
        'name' => $username,
    ]);
    
    return $users;
  }
  
  /**
   * Return all enterprises enabled.
   * 
   * @return \Drupal\Core\Entity\EntityInterface[]
   */
  public function getEnterprises() {
    $node_storage = $this->entityTypeManager->getStorage('node');
    $enterprises = $node_storage->loadByProperties(['type' => 'enterprise', 'status' => 1]);
    
    $this->logger->info(
      $this->t('[@current_user] - All enterprise returned',[
        '@current_user' => $this->current_user->id()]));
    
    return $enterprises;
  }
  
  /**
   * Return users array with 'efichajes enterprise admin' permission.
   * 
   * @return array
   */
  public function getUserAdmin() {
    $user_storage = $this->entityTypeManager->getStorage('user');
    $users = $user_storage->loadMultiple(null);
    
    $result = [];
    foreach ($users as $key => $value) {
      if ($value->hasPermission('efichajes enterprise admin') && $value->id() != 1) {
        $result[$key]['surnames'] = $value->get('field_worker_surnames')->value;
        $result[$key]['name'] = $value->get('field_worker_name')->value;
        $result[$key]['id'] = $value->get('field_worker_id')->value;
      }
    }
    
    $this->logger->info(
      $this->t('[@current_user] - All users with "efichajes enterprise admin" permission returned.',[
        '@current_user' => $this->current_user->id()]));
    
    return $result;
  }
  
  /**
   * Return admin users from a enterprise.
   * 
   * {@inheritDoc}
   * @see \Drupal\efichajes\EfichajesDatabaseInterface::getAdminsFromEnterprise()
   */
  public function getAdminsFromEnterprise($enterprise) {
    $query = $this->database->select('efichajes_enterprise_users', 'a')
    ->fields('a', ['uid'])
    ->condition('a.nid', $enterprise, '=');
    
    $result = $query->execute()->fetchAllAssoc('uid', \PDO::FETCH_ASSOC);
    
    $this->logger->info(
      $this->t('[@current_user] - Admin users assigned to @enterprise enterprise returned.',[
        '@current_user' => $this->current_user->id(),
        '@enterprise' => $enterprise]));
    
    return $result;
  }
  
  /**
   * Assign user to enterprise as admin.
   * 
   * {@inheritDoc}
   * @see \Drupal\efichajes\EfichajesDatabaseInterface::insertUserEnterprise()
   */
  public function insertUserEnterprise($enterprise, $user) {
    $result = $this->database->insert('efichajes_enterprise_users')
    ->fields([
      'nid' => $enterprise,
      'uid' => $user,
    ])
    ->execute();
    
    $this->logger->info(
      $this->t('[@current_user] User @user assigned to Enterprise @enterprise', [
        '@current_user' => $this->current_user->id(),
        '@user' => $user,
        '@enterprise' => $enterprise ]));
  }
  
  /**
   * Remove user from enterprise as admin.
   * 
   * {@inheritDoc}
   * @see \Drupal\efichajes\EfichajesDatabaseInterface::deleteUserEnterprise()
   */
  public function deleteUserEnterprise($enterprise, $user) {
    $result = $this->database->delete('efichajes_enterprise_users')
    ->condition('nid', $enterprise)
    ->condition('uid', $user)
    ->execute();
    
    $this->logger->info(
      $this->t('[@current_user] User @user deleted from Enterprise @enterprise', [
        '@current_user' => $this->current_user->id(),
        '@user' => $user,
        '@enterprise' => $enterprise ]));
  }
  
  /**
   * Return true if user is assigned to enterprise
   * 
   * @param unknown $user
   * @param unknown $enterprise
   * @return boolean
   */
  public function isUserEnterprise($user, $enterprise) {
    $query = $this->database
    ->select('efichajes_enterprise_users', 'e')
    ->condition('nid', $enterprise)
    ->condition('uid', $user);
    $query->addExpression('count(*)', 'counter');
    
    $result = $query->execute();
    $count = $result->fetchField();
    
    $this->logger->info(
      $this->t('[@current_user] @count Users assigned to Enterprise @enterprise', [
        '@current_user' => $this->current_user->id(),
        '@count' => $count, 
        '@user' => $user,
        '@enterprise' => $enterprise ]));
    
    if ($count != 0) return true;
    else return false;
  }
  
  /**
   * Return true if users exists (Searching user by id).
   * 
   * @param string $worker_id
   * @return boolean
   */
  public function existUser($worker_id) {
    $user_storage = $this->entityTypeManager->getStorage('user');
    $values = [
      'field_worker_id' => $worker_id,
    ];
    $users = $user_storage->loadByProperties($values);
    $count = count($users);
    
    $this->logger->info(
      $this->t('[@current_user] @count Users found with id @worker_id', [
        '@current_user' => $this->current_user->id(),
        '@count' => $count,
        '@worker_id' => $worker_id ]));
    
    if (empty($users)) return FALSE;
    else return TRUE;
  }
  
  /**
   * Return uid from user by id.
   * 
   * @param string $worker_id
   * @return int
   */
  public function getUid($worker_id) {
    $user_storage = $this->entityTypeManager->getStorage('user');
    $values = [
      'field_worker_id' => $worker_id,
    ];
    $users = $user_storage->loadByProperties($values);
    $count = count($users);
    
    $result = [];
    foreach ($users as $user) {
      $result[] = $user->id();
    }
    
    return $result;
  }
  
  /**
   * Return all signings types.
   * @return array
   */
  public function getSignings() {
    $query = $this->database->select('efichajes_signings_type', 'e')
    ->fields('e', ['stid', 'description', 'status'])
    ->orderBy('e.stid', 'asc');
    
    $result = $query->execute()->fetchAllAssoc('stid', \PDO::FETCH_ASSOC);
    $this->logger->info(
      $this->t('[@current_user] Return getSignings.', [
        '@current_user' => $this->current_user->id() ]));
    
    return $result;
  }
  
  /**
   * Return signing by id
   * @param int $id
   * @return array
   */
  public function getSigning($id) {
    $query = $this->database->select('efichajes_signings_type','e')
    ->fields('e', ['stid', 'description', 'status'])
    ->condition('e.stid', $id, '=');
    
    $result = $query->execute()->fetchAssoc();
    $this->logger->info(
      $this->t('[@current_user] Return signing with id @id.', [
        '@current_user' => $this->current_user->id(),
        '@id' => $id ]));
    
    return $result;
  }
  
  /**
   * Update signing type.
   * @param int $stid
   * @param string $description
   * @param int $status
   */
  public function updateSigning($stid, $description, $status) {
    $this->database->update('efichajes_signings_type')
    ->fields([
      'description' => $description,
      'status' => $status,
    ])
    ->condition('stid', $stid, '=')
    ->execute();
    
    $this->logger->info(
      $this->t('[@current_user] Signing Updated - @stid | @description | @status', [
        '@current_user' => $this->current_user->id(),
        '@stid' => $stid,
        '@description' => $description,
        '@status' => $status ]));
  }
  
  /**
   * Insert signing type.
   * @param string $description
   * @param int $status
   */
  public function insertSigning($description, $status) {
    $this->database->insert('efichajes_signings_type')
    ->fields([
      'description' => $description,
      'status' => $status,
    ])
    ->execute();
    
    $this->logger->info(
      $this->t('[@current_user] Signing Inserted - @description | @status', [
        '@current_user' => $this->current_user->id(),
        '@description' => $description,
        '@status' => $status ]));
  }
  
  /**
   * Delete signing type.
   * @param int $stid
   */
  public function deleteSigning($stid) {
    $query = $this->database->select('efichajes_signings', 'a')
    ->condition('a.stid', $stid, '=');
    
    $result = $query->countQuery()->execute()->fetchField();
    if ($result != 0) {
      $signing = $this->getSigning($stid);
      $this->updateSigning($signing['stid'], $signing['description'], '0');
    } else {
      $this->database->delete('efichajes_signings_type')
      ->condition('stid', $stid, '=')
      ->execute();
      
      $this->logger->info(
        $this->t('[@current_user] Signing Deleted - @stid', [
          '@current_user' => $this->current_user->id(),
          '@stid' => $stid ]));
    }
  }
  
  /**
   * Update signing status.
   * @param int $stid
   */ 
  public function changeStatusSigning($stid) {
    $signing = $this->getSigning($stid);
    $this->updateSigning($stid, $signing['description'], ($signing['status']) ? 0 : 1);
  }
  
  /**
   * Return all signings enabled.
   * @return array
   */
  public function getEnabledSignings() {
    $query = $this->database->select('efichajes_signings_type', 'est')
    ->fields('est', ['stid', 'description'])
    ->condition('est.status','1','=');
    
    $signings = $query->execute()->fetchAllAssoc('stid', \PDO::FETCH_ASSOC);
    
    $this->logger->info(
      $this->t('[@current_user] All signing returned', [
        '@current_user' => $this->current_user->id()]));
    
    return $signings;
  }
  
  /**
   * Return enabled signing for an enterprise
   * @param int $nid
   * @return array
   */
  public function getEnterpriseSignings($nid) {
    $query = $this->database->select('efichajes_enterprise_signings_type', 'eest')
    ->fields('eest', ['stid'])
    ->condition('eest.nid',$nid, '=')
    ->condition('eest.status', '1', '=');
    
    $result = $query->execute()->fetchAllAssoc('stid', \PDO::FETCH_ASSOC);
    $default_signings = [];
    foreach ($result as $key => $value) {
      $default_signings[$key] = TRUE;
    }
    
    $this->logger->info(
      $this->t('[@current_user] All signings for enterprise @enterprise returned', [
        '@current_user' => $this->current_user->id(),
        '@enterprise' => $nid]));
    
    return $default_signings;
  }
  
  /**
   * Insert/Update signing for enterprise
   * @param integer $nid
   * @param integer $stid
   */
  public function addSigningEnterprise($nid, $stid) {
    $query = $this->database->select('efichajes_enterprise_signings_type', 'eest')
    ->condition('eest.nid', $nid, '=')
    ->condition('eest.stid', $stid, '=');
    
    $result = $query->countQuery()->execute()->fetchField();
    if ($result != 0) {
      $this->database->update('efichajes_enterprise_signings_type')
      ->fields([
          'status' => 1,
      ])
      ->condition('stid', $stid, '=')
      ->condition('nid', $nid, '=')
      ->execute();
      
      $this->logger->info(
        $this->t('[@current_user] Signing @signing_id for enterprise @enterprise updated', [
          '@current_user' => $this->current_user->id(),
          '@signing_id' => $stid,
          '@enterprise' => $nid]));
    } else {
      $this->database->insert('efichajes_enterprise_signings_type')
      ->fields([
          'stid' => $stid,
          'nid' => $nid,
          'status' => '1',
      ])
      ->execute();
      
      $this->logger->info(
        $this->t('[@current_user] Signing @signing_id for enterprise @enterprise inserted', [
          '@current_user' => $this->current_user->id(),
          '@signing_id' => $stid,
          '@enterprise' => $nid]));
    }
  }
  
  /**
   * Remove/Update signing for enterprise
   * @param integer $nid
   * @param integer $stid
   */
  public function removeSigningEnterprise($nid, $stid) {
    $query = $this->database->select('efichajes_signings', 'a')
    ->condition('a.nid', $nid, '=')
    ->condition('a.stid', $stid, '=');
    
    $result = $query->countQuery()->execute()->fetchField();
    if ($result != 0) {
      update('efichajes_enterprise_signings_type')
      ->fields([
          'status' => '1',
      ])
      ->condition('stid', $stid)
      ->condition('nid', $nid)
      ->execute();
      
      $this->logger->info(
        $this->t('[@current_user] Signing @signing_id for enterprise @enterprise updated', [
          '@current_user' => $this->current_user->id(),
          '@signing_id' => $stid,
          '@enterprise' => $nid]));
    } else {
      $this->database->delete('efichajes_enterprise_signings_type')
      ->condition('stid', $stid)
      ->condition('nid', $nid)
      ->execute();
      
      $this->logger->info(
        $this->t('[@current_user] Signing @signing_id for enterprise @enterprise deleted', [
          '@current_user' => $this->current_user->id(),
          '@signing_id' => $stid,
          '@enterprise' => $nid]));
    }
  }
  
  /**
   * Return all signings types enabled for enterprise
   * @param int $nid
   * @return array
   */
  public function getEnterpriseSigningstype($nid) {
    $query = $this->database
    ->select('efichajes_enterprise_signings_type', 'a')
    ->fields('a', ['stid'])
    ->condition('a.status', '1', '=');
    
    $query->leftJoin('efichajes_signings_type', 'b', 'a.stid = b.stid');
    $query->fields('b', ['description']);
    
    $result = $query->execute()->fetchAllAssoc('stid', \PDO::FETCH_ASSOC);
    $this->logger->info(
      $this->t('[@current_user] All signings type for enterprise @enterprise returned', [
        '@current_user' => $this->current_user->id(),
        '@enterprise' => $nid]));

    return $result;
  }
  
  /**
   * Insert worker signing.
   * @param int $nid
   * @param int $uid
   * @param int $stid
   * @param string $ip_client
   * @param string $browser_info
   */
  public function insertWorkerSigning($nid, $uid, $stid, $ip_client, $browser_info) {
    $time = time();
    
    $result = $this->database->insert('efichajes_signings')
    ->fields([
        'nid' => $nid,
        'uid' => $uid,
        'stid' => $stid,
        'signing_date' => $time,
        'ip_client' => $ip_client,
        'browser_info' => $browser_info,
    ])
    ->execute();
    
    $this->logger->info(
      $this->t('[@current_user] Worker signing inserted - @stid - @ip - @browserinfo', [
        '@current_user' => $this->current_user->id(),
        '@stid' => $stid,
        '@ip' => $ip_client,
        '@browserinfo' => $browser_info]));
  }
  
  /**
   * Return all signings for a user/enterprise/day
   * @param int $nid
   * @param int $uid
   * @param int $time
   * @return array
   */
  public function getDaySignings($nid, $uid, $time) {
    $current_date = date('d/m/Y', $time);
    
    $query = $this->database->select('efichajes_signings', 'a')
    ->fields('a', ['stid']);
    $query->addExpression('from_unixtime(a.signing_date, \'%d/%m/%Y %H:%i:%s\')','signing_date');
    $query->leftJoin('efichajes_signings_type', 'b', 'a.stid = b.stid');
    $query->fields('b',['description']);
    $query->where('from_unixtime(a.signing_date, \'%d/%m/%Y\') = :current_date', [
        ':current_date' => $current_date,
    ]);
    
    $result = $query->execute()->fetchAllAssoc('signing_date', \PDO::FETCH_ASSOC);
    
    return $result;
  }
}