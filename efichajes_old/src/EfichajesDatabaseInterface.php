<?php

namespace Drupal\efichajes;

interface EfichajesDatabaseInterface {
  public function getUserByUsername($username);
  public function getEnterprises();
  public function getUserAdmin();
  public function getAdminsFromEnterprise($enterprise);
  public function insertUserEnterprise($enterprise, $user);
  public function deleteUserEnterprise($enterprise, $user);
  public function isUserEnterprise($user, $enterprise);
  public function existUser($worker_id);
  public function getUid($worker_id);
  public function getSignings();
  public function getSigning($id);
  public function updateSigning($stid, $description, $status);
  public function insertSigning($description, $status);
  public function deleteSigning($stid);
  public function changeStatusSigning($stid);
  public function getEnabledSignings();
  public function getEnterpriseSignings($nid);
  public function addSigningEnterprise($nid, $stid);
  public function removeSigningEnterprise($nid, $stid);
  public function getEnterpriseSigningstype($nid);
  public function insertWorkerSigning($nid, $uid, $stid, $ip_client, $browser_info);
  public function getDaySignings($nid, $uid, $time);
}