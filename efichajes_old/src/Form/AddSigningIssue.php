<?php

namespace Drupal\efichajes\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Database\Connection;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Psr\Log\LoggerInterface;
use Drupal\Core\Form\FormBuilderInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\node\NodeInterface;
use Drupal\user\UserInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\CloseModalDialogCommand;
use Drupal\Core\Ajax\ReplaceCommand;
use Drupal\Core\Ajax\OpenModalDialogCommand;

class AddSigningIssue extends FormBase {
  protected $current_user;
  protected $database;
  protected $entityTypeManager;
  protected $logger;
  protected $node;
  protected $user;
  
  public function __construct(  AccountInterface $current_user,
                                Connection $database, 
                                EntityTypeManagerInterface $entityTypeManager,
                                LoggerInterface $logger, 
                                FormBuilderInterface $form_builder) {
    $this->current_user = $current_user;
    $this->database = $database;
    $this->entityTypeManager = $entityTypeManager;
    $this->logger = $logger;
    $this->form_builder = $form_builder;
  }
  
  public static function create (ContainerInterface $container) {
    return new static (
      $container->get('current_user'),
      $container->get('database'),
      $container->get('entity_type.manager'),
      $container->get('logger.factory')->get('efichajes - SigningsIssue'),
      $container->get('form_builder')
    );
  }
  
  public function getFormId() {
    return 'efichajes_addsigningissue_form';
  }
  
  protected function getIssueType() {
    $query = $this->database->select('efichajes_signings_issue_type', 'a');
    $query->fields('a', ['sitid', 'description']);
    $query->condition('a.status', '1', '=');
    
    $result = $query->execute()->fetchAllAssoc('sitid', \PDO::FETCH_ASSOC);
    $issuetype = [];
    foreach ($result as $key => $value) {
      $issuetype[$key] = $value['description'];
    }
    return $issuetype;
  }
  
  protected function getSigningstype($nid) {
    $query = $this->database
    ->select('efichajes_enterprise_signings_type', 'a')
    ->fields('a', ['stid'])
    ->condition('a.status', '1', '=');
  
    $query->leftJoin('efichajes_signings_type', 'b', 'a.stid = b.stid');
    $query->fields('b', ['description']);
  
    $result = $query->execute()->fetchAllAssoc('stid', \PDO::FETCH_ASSOC);
    $signing = [];
    foreach ($result as $key => $value) {
      $signing[$key] = $key . ' - ' . $value['description'];
    }
  
    return $signing;
  }
  
  protected function addissue($nid, $sid, $sitid, $as_date, $as_type, $as_reason, $uid, $sisid) {
    $this->database->insert('efichajes_signings_issue')
    ->fields([
      'nid' => $nid,
      'sid' => $sid,
      'sitid' => $sitid,
      'alter_signing_date' => $as_date,
      'alter_signing_type' => $as_type,
      'alter_signing_reason' => $as_reason,
      'uid' => $uid,
      'sisid' => $sisid,
    ])
    ->execute();
  }
  
  public function ajaxAddIssue(array &$form, FormStateInterface $form_state) {
    /*
    $this->addissue(  $node->id(),
                      null,
                      '0',
                      $as_date,
                      $form_state->getValue('signing_type'),
                      $form_state->getValue('reason'),
                      $user->id(),
                      $sisid);
    */
    $response = new AjaxResponse();
    //$response->addCommand(new ReplaceCommand('#add-issue-form', $form));
    $response->addCommand(new OpenModalDialogCommand('Success!!!', 'Issue Added.'));
    
    return $response;
  }
  
  public function buildForm(  array $form, 
                              FormStateInterface $form_state, 
                              NodeInterface $node = NULL,
                              UserInterface $user = NULL,
                              DrupalDateTime $date = NULL) {
    $this->node = $node;
    $this->user = $user;
    
    $form['#prefix'] = '<div id="add-issue-form">';
    $form['#suffix'] = '</div>';
    
    $form['date'] = [
      '#type' => 'datetime',
      '#title' => $this->t('Hour'),
      '#description' => $this->t('Intro new time for signing.'),
      '#default_value' => $date,
      '#date_date_element' => 'none',
    ];
    
    $form['signing_issue_type'] = [
      '#type' => 'select',
      '#title' => $this->t('Issue Type'),
      '#description' => $this->t('Select Issue Type'),
      '#options' => $this->getIssueType(),
      '#default_value' => '0',
      '#disabled' => TRUE,
    ];
    
    $form['signing_type'] = [
      '#type' => 'select',
      '#title' => $this->t('Signing Type'),
      '#description' => $this->t('Select new signing type.'),
      '#options' => $this->getSigningstype($node->id()),
    ];
    
    $form['reason'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Reason'),
      '#description' => $this->t('Intro your reason to add this new signing.'),
      '#size' => 150,
    ];
    
    $form['actions'] = [
      '#type' => 'actions',
    ];
    
    $form['actions']['add'] = [
      '#type' => 'submit',
      '#value' => $this->t('Add'),
      '#ajax' => [
        'callback' => [$this, 'ajaxAddIssue'],
        'event' => 'click',
      ],
    ];
    
    $form['#attached']['library'][] = 'core/drupal.dialog.ajax';
    
    return $form;
  }
  
  public function submitForm(array &$form, FormStateInterface $form_state) { }
}