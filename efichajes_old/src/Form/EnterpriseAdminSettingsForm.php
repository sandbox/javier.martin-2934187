<?php 

namespace Drupal\efichajes\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\efichajes\EfichajesDatabaseInterface;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\ReplaceCommand;

/**
 * Form to assign users to enterprise as Admins.
 * @author fjmlillo
 *
 */

class EnterpriseAdminSettingsForm extends FormBase {
  protected $efichajes_database;
  
  public function __construct(EfichajesDatabaseInterface $efichajes_database) {
    $this->efichajes_database = $efichajes_database;
  }
  
  public static function create(ContainerInterface $container) {
    return new static (
      $container->get('efichajes.database')
    );
  }
  
  public function getFormId() {
    return 'efichajes_enterpriseadminsettings_form';
  }
  
  public function selectEnterpriseCallback(array &$form, FormStateInterface $form_state) {
    $enterprise = $form_state->getValue('enterprise');
    $users = $this->efichajes_database->getUserAdmin();
    $default_users = $this->efichajes_database->getAdminsFromEnterprise($enterprise);
    
    foreach ($users as $key => $value) {
      if (array_key_exists($key, $default_users)) {
        $form['users_container']['users'][$key]['#checked'] = TRUE;
      } else {
        unset($form['users_container']['users'][$key]['#checked']);
      }
    }
    
    $response = new AjaxResponse();
    $response->addCommand(new ReplaceCommand('#users', $form['users_container']));
    
    return $response;
  }
  
  public function checkUserCallback(array &$form, FormStateInterface $form_state) {
    $response = new AjaxResponse();
    $response->addCommand(new ReplaceCommand('#users', $form['users_container']));
    
    return $response;
  }
  
  public function buildForm(array $form, FormStateInterface $form_state) {
    // Node storage to get all enterprise published
    $enterprises = $this->efichajes_database->getEnterprises();
    
    // Create array with enterprise to select form element
    $options_enterprise = array();    
    foreach ($enterprises as $key => $value) {
      $options_enterprise[$key] = $value->getTitle(); 
    }
    
    // Get default option for enterprise select
    reset($options_enterprise);
    $default_value_enterprise = key($options_enterprise);
    
    $element = $form_state->getTriggeringElement();
    if (empty($element)) {
      // Load Form
      $users = $this->efichajes_database->getUserAdmin();
      $default_users = $this->efichajes_database->getAdminsFromEnterprise($default_value_enterprise);
    } else if (!empty($element) && $element['#type'] == 'select') {
      // Click select new enterprise
      $enterprise = $form_state->getValue('enterprise');
      $users = $this->efichajes_database->getUserAdmin();
      $default_users = $this->efichajes_database->getAdminsFromEnterprise($enterprise);
    } else if (!empty($element) && $element['#type'] == 'checkbox') {
      // Click checkbox user
      $enterprise = $form_state->getValue('enterprise');
      $user = $element['#return_value'];
      if ($element['#default_value'] == $element['#return_value']) {
        // Check User
        $this->efichajes_database->insertUserEnterprise($enterprise, $user);
      } else {
        // Uncheck User
        $this->efichajes_database->deleteUserEnterprise($enterprise, $user);
      }
      $users = $this->efichajes_database->getUserAdmin();
      $default_users = $this->efichajes_database->getAdminsFromEnterprise($enterprise);
    }
    
    $form['enterprise'] = [
      '#type' => 'select',
      '#title' => $this->t('Select enterprise'),
      '#description' => $this->t('Select enterprise to assign admin users.'),
      '#options' => $options_enterprise,
      '#ajax' => [
        'callback' => [$this, 'selectEnterpriseCallback'],
      ],
      '#default_value' => $default_value_enterprise,
    ];
    
    $form['users_container'] = [
      '#type' => 'container',
      '#attributes' => [ 'id' => 'users'],
    ];
    
    $header = [
      'id' => $this->t('Worker Id'),
      'surnames' => $this->t('Surnames'),
      'name' => $this->t('Name'),
    ];
    
    $form['users_container']['users'] = [
      '#type' => 'tableselect',
      '#header' => $header,
      '#options' => $users,
      '#default_value' => $default_users,
      '#empty' => $this->t('No users found'),
      '#multiple' => TRUE,
      '#ajax' => [
        'callback' => [$this, 'checkUserCallback'],
      ],
    ];
    
    return $form;
  }
  
  public function submitForm(array &$form, FormStateInterface $form_state) { }
}
