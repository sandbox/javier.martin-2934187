<?php
namespace Drupal\efichajes\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Session\AccountInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\efichajes\EfichajesDatabaseInterface;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\ReplaceCommand;

class EnterpriseSigningsTypesForm extends FormBase {
  protected $current_user;
  protected $efichajesDatabase;
  
  public function __construct(AccountInterface $current_user, EfichajesDatabaseInterface $efichajesDatabase) {
        $this->current_user = $current_user;
        $this->efichajesDatabase = $efichajesDatabase;
  }
  
  public static function create (ContainerInterface $container) {
    return new static (
      $container->get('current_user'),
      $container->get('efichajes.database')
    );
  }
  
  public function getFormId() {
    return 'efichajes_enterprisesigningtypes_form';
  }
  
  public function ajaxTableSelect(array $form, FormStateInterface $form_state) {
    $response = new AjaxResponse();
    $response->addCommand(new ReplaceCommand('#signings-container', $form['signings_container']));
    
    return $response;
  }
  
  public function ajaxSelect(array $form, FormStateInterface $form_state) {
    $enterprise = $form_state->getValue('enterprise');
    $signings = $this->efichajesDatabase->getSignings();
    $default_signings = $this->efichajesDatabase->getEnterpriseSignings($enterprise);
    
    foreach ($signings as $key => $value) {
      if (array_key_exists($key, $default_signings)) {
        $form['signings_container']['signings'][$key]['#checked'] = TRUE;
      } else {
        unset($form['signings_container']['signings'][$key]['#checked']);
      }
    }
    
    $response = new AjaxResponse();
    $response->addCommand(new ReplaceCommand('#signings-container', $form['signings_container']));
    
    return $response;
  }
  
  public function buildForm(array $form, FormStateInterface $form_state) {
    
    $enterprises = $this->efichajesDatabase->getEnterprises();
    $options_enterprise = [];
    foreach ($enterprises as $key => $value) {
      if ($this->efichajesDatabase->isUserEnterprise($this->current_user->id(), $key) ||
          $this->current_user->id() == 1) {
        $options_enterprise[$key] = $value->getTitle();
      }
    }
    reset($options_enterprise);
    $default_enterprise = key($options_enterprise);
    
    $element = $form_state->getTriggeringElement();
    if (empty($element)) {
      // Fill Tableselect
      $options_signings = $this->efichajesDatabase->getEnabledSignings();
      $default_signings = $this->efichajesDatabase->getEnterpriseSignings($default_enterprise);
    } else if (!empty($element) && $element['#type'] == 'checkbox') {
      $enterprise = $form_state->getValue('enterprise');
      $stid = $element['#return_value'];
      if ($element['#return_value'] == $element['#default_value']) {
        // Add Signing to enterprise
        $this->efichajesDatabase->addSigningEnterprise($enterprise, $stid);
      } else {
        $this->efichajesDatabase->removeSigningEnterprise($enterprise, $stid);
      }
      
      // Fill Tableselect
      $enterprise = $form_state->getValue('enterprise');
      $options_signings = $this->efichajesDatabase->getEnabledSignings();
      $default_signings = $this->efichajesDatabase->getEnterpriseSignings($enterprise);
    } else if (!empty($element) && $element['#type'] == 'select') {
      $enterprise = $form_state->getValue('enterprise');
      $options_signings = $this->efichajesDatabase->getEnabledSignings();
      $default_signings = $this->efichajesDatabase->getEnterpriseSignings($enterprise);
    }
    
    $form['form_description'] = [
      '#markup' => $this->t('
        Use this form to assign signing type to enterprise in order to allow
        his workers to use it.'),
    ];
    
    $form['enterprise'] = [
      '#type' => 'select',
      '#title' => $this->t('Select enterprise'),
      '#description' => $this->t('Select enterprise to import workers.'),
      '#options' => $options_enterprise,
      '#default_value' => empty($options_enterprise) ? null : $default_enterprise,
      '#ajax' => [
        'callback' => [$this, 'ajaxSelect'],
      ],
    ];
    
    $form['signings_container'] = [
      '#type' => 'container',
      '#attributes' => ['id' => 'signings-container'],
    ];
    
    $header = [
      'stid' => $this->t('Signing Id'),
      'description' => $this->t('Description')
    ];
    
    $form['signings_container']['signings']  = [
      '#type' => 'tableselect',
      '#header' => $header,
      '#options' => $options_signings,
      '#default_value' => $default_signings,
      '#empty' => $this->t('No signings found'),
      '#multiple' => TRUE,
      '#ajax' => [
        'callback' => [$this, 'ajaxTableSelect'],
      ],
    ];
    
    return $form;
  }
  
  public function submitForm(array &$form, FormStateInterface $form_state) { }
}