<?php 

namespace Drupal\efichajes\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Session\AccountInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Psr\Log\LoggerInterface;
use Drupal\file\FileUsage\DatabaseFileUsageBackend;
use Drupal\Core\File\FileSystemInterface;
use Drupal\efichajes\EfichajesDatabaseInterface;
use Drupal\file\FileInterface;

/**
 * Form to import workers into enterprise admin by current user.
 * @author fjmlillo
 *
 */

class ImportWorkersForm extends FormBase {
  protected $current_user;
  protected $entityTypeManager;
  protected $logger;
  protected $file_usage;
  protected $file_system;
  protected $efichajes_database;
  
  public function __construct(AccountInterface $current_user, 
      EntityTypeManagerInterface $entity_type_manager,
      LoggerInterface $logger,
      DatabaseFileUsageBackend $file_usage, 
      FileSystemInterface $file_system,
      EfichajesDatabaseInterface $efichajes_database) {
    $this->current_user = $current_user;
    $this->entityTypeManager = $entity_type_manager;
    $this->logger = $logger;
    $this->file_usage = $file_usage;
    $this->file_system = $file_system;
    $this->efichajes_database = $efichajes_database;
  }
  
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('current_user'),
      $container->get('entity_type.manager'),
      $container->get('logger.factory')->get('eFichajes - ImportWorkersForm'),
      $container->get('file.usage'),
      $container->get('file_system'),
      $container->get('efichajes.database')
    );
  }
  
  public function getFormId() {
    return 'efichajes_importworkers_form';
  }
  
  /**
   * Process file creating users.
   */
  protected function processFile($path, $enterprise) {
    $user_storage = $this->entityTypeManager->getStorage('user');
    $counter = 0; $created = 0; $repeated = 0; $added = 0;
    $fp = fopen($path, 'r');
    
    // Until then end of file exe
    while (!feof($fp)) {
      $counter++;
      $linea = fgets($fp);
      $campos = explode(';', $linea);
      $valid_email = (isset($campos[3]) ) ?
      preg_match('/^[A-z0-9\\._-]+@[A-z0-9][A-z0-9-]*(\\.[A-z0-9_-]+)*\\.([A-z]{2,6})$/', $campos[3]) : FALSE ;

      if (sizeof($campos) == 4 && $valid_email) {
        $values = [
          'field_worker_id' => $campos[0],
          'field_worker_name' => $campos[1],
          'field_worker_surnames' => $campos[2],
          'roles' => ['efichajes_worker'],
          'mail' => $campos[3],
          'name' => $campos[0],
          'pass' => substr( md5(microtime()), 1, 8),
          'status' => 1
        ];
        
        if (!$this->efichajes_database->existUser($values['field_worker_id'])) {
          // if not exist user with this id -> add user and assign to enterprise
          $user = $user_storage->create($values);
          $user->save();
          
          $uids = $this->efichajes_database->getUid($values['field_worker_id']);
          $uid = current($uids);
          $user = $user_storage->load($uid);
          
          $this->efichajes_database->insertUserEnterprise($enterprise, $user->id());
          
          $created++;
          
          drupal_set_message($this->t('User created on line @counter with id = @worker_id.',
              ['@counter' => $counter, '@worker_id' => $values['field_worker_id']]));
          
          $this->logger->info(
              $this->t('Actual User: @uid. User Created. Id: @id Enterprise: @nid', 
                  ['@uid' => $this->current_user->id(),'@id' => $values['field_worker_id'], '@nid' => $enterprise])
          );
        } else {
          // if user exist
          $uids = $this->efichajes_database->getUid($values['field_worker_id']);
          $uid = $uids[0];
          
          // Check if user is assign to enterprise
          // -> No assign -> Assign to enterprise
          // -> Assign -> Report duplicate user
          if (!$this->efichajes_database->isUserEnterprise($uid, $enterprise)) {
            $added++;
            
            $this->efichajes_database->insertUserEnterprise($enterprise, $uid);
            
            drupal_set_message($this->t('User added on line @counter with id = @worker_id.',
                ['@counter' => $counter, '@worker_id' => $values['field_worker_id']]));
            
            $this->logger->info(
              $this->t('Actual User: @uid. User added to enterprise. Id: @id Enterprise: @nid', 
                  ['@uid' => $this->current_user->id(), '@id' => $values['field_worker_id'], '@nid' => $enterprise])
            );
          } else {
            $repeated++;
            
            drupal_set_message($this->t('User exist on line @counter with id = @worker_id.',
                ['@counter' => $counter, '@worker_id' => $values['field_worker_id']]));
            
            $this->logger->info(
              $this->t('Actual User: @uid. User Repeated. Id: @id Enterprise: @nid', 
                  ['@uid' => $this->current_user->id(), '@id' => $values['field_worker_id'], '@nid' => $enterprise])
            );
          }
        }
      } else {
        drupal_set_message($this->t('Record incorrect on line @counter.', ['@counter' => $counter]));
      }
    }
    fclose($fp);
  
    drupal_set_message($this->t('Summary:'));
    drupal_set_message($this->t('@counter lines processed', ['@counter' => $counter]));
    drupal_set_message($this->t('@repeated users repeated found', ['@repeated' => $repeated]));
    drupal_set_message($this->t('@created users created', ['@created' => $created]));
  
    if ($created != 0) return TRUE;
    else return FALSE;
  }
  
  
  
  public function buildForm(array $form, FormStateInterface $form_state) {
    $enterprises = $this->efichajes_database->getEnterprises();
    
    $options_enterprise = [];
    foreach ($enterprises as $key => $value) {
      if ($this->efichajes_database->isUserEnterprise($this->current_user->id(), $key) ||
          $this->current_user->hasPermission('efichajes module admin') ||
          $this->current_user->id() == 1) {
        $options_enterprise[$key] = $value->getTitle();
      }
    }
    
    $form['form_description'] = [
      '#markup' => $this->t('
        Use this form in order to import worker and assign then to enterprise
        selected.'),
    ];
    
    $form['enterprise'] = [
      '#type' => 'select',
      '#title' => $this->t('Select enterprise'),
      '#description' => $this->t('Select enterprise to import workers.'),
      '#options' => $options_enterprise,
    ];
    
    $form['workerfile'] = [
      '#type' => 'managed_file',
      '#title' => $this->t('Worker File'),
      '#description' => $this->t('Select file in csv format. All field must be
          separated with comma and format must be: Id number, name, last name, email'),
      '#upload_location' => 'private://efichajes/importworkers',
      '#upload_validators' => [
          'file_validate_extensions' => ['csv'],
      ],
      '#required' => TRUE,
    ];
    
    $form['actions'] = [ '#type' => 'actions'];
    
    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Submit'),
    ];
    
    return $form;
  }
  
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $enterprise = $form_state->getValue('enterprise');
    $workerfiles = $form_state->getValue('workerfile');
    
    $file_storage = $this->entityTypeManager->getStorage('file');
    
    foreach($workerfiles as $fid) {
      $file = $file_storage->load($fid);
      $path = $this->file_system->realpath($file->get('uri')->value);
      if ($this->processFile($path, $enterprise)) {
        $this->file_usage->add($file, 'efichajes', 'node', $enterprise);
        $this->logger->info(
          $this->t('Actual User: @uid. File @file added to enterprise @enterprise', 
              ['@uid' => $this->current_user->id(), '@file' => $file->get('filename')->value, '@enterprise' => $enterprise])
        );
      }
    }
    
  }
 }