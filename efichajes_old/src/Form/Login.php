<?php 

namespace Drupal\efichajes\Form;


use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\node\NodeInterface;
use Drupal\Core\Session\AccountInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Password\PasswordInterface;
use Drupal\Core\Database\Connection;
use Drupal\efichajes\EfichajesDatabaseInterface;

class Login extends FormBase {
  protected $current_user;
  protected $logger;
  protected $passwordHasher;
  protected $efichajesDatabase;
  
  public function __construct(AccountInterface $current_user, LoggerInterface $logger, 
    PasswordInterface $passwordhasher, EfichajesDatabaseInterface $efichajesDatabase) {
    $this->current_user = $current_user;
    $this->logger = $logger;
    $this->passwordHasher = $passwordhasher;
    $this->efichajesDatabase = $efichajesDatabase;
  }
  
  public static function create(ContainerInterface $container) {
    return new static (
      $container->get('current_user'),
      $container->get('logger.factory')->get('efichajes - LoginForm'),
      $container->get('password'),
      $container->get('efichajes.database')
    );
  }
  
  public function getFormId() {
    return 'efichajes_login_form';
  }
  
  public function buildForm(array $form, FormStateInterface $form_state, NodeInterface $node = NULL) {
    if (empty($node) || $node->getType() != 'enterprise') {
      $form['enterprise'] = [
        '#markup' => $this->t('Error on form URL.'),
        '#prefix' => '<p>',
        '#suffix' => '</p>',
      ];
    } else {
      $form_state->set('enterprise', $node);
      $form['enterprise'] = [
        '#markup' => '<p>' . 
          $this->t('Welcome to') .
          ' <strong>'. $node->getTitle() . '</strong> ' .
          $this->t('Login Web') . '</p>',
      ];
      
      $form['username'] = [
        '#type' => 'textfield',
        '#title' => $this->t('Login'),
        '#description' => $this->t('Intro your login')
      ];
      
      $form['password'] = [
        '#type' => 'password',
        '#title' => $this->t('Password'),
        '#description' => $this->t('Intro password'),
      ];
      
      $form['actions'] = [
        '#type' => 'actions',
      ];
      
      $form['actions']['submit'] = [
        '#type' => 'submit',
        '#value' => $this->t('Submit'),
      ];
      
    }
    
    return $form;
  }
  
  public function validateForm(array &$form, FormStateInterface $form_state) {
    $username = $form_state->getValue('username');
    $password = $form_state->getValue('password');
    $enterprise = $form_state->get('enterprise');
    
    $users = $this->efichajesDatabase->getUserByUsername($username);
    if (!empty($users)) {
      $user = current($users);
      
      if (!$this->efichajesDatabase->isUserEnterprise($user->id(),$enterprise->id())) {
        $form_state->setErrorByName(
          'Enterprise',
          $this->t('User not assigned to enterprise')
        );
      }
      
      if (!$user->hasPermission('efichajes worker') && !$user->hasPermission('efichajes enterprise admin')) {
        $form_state->setErrorByName(
          'Permission',
          $this->t('Incorrect permission assigned to user.')
        );
      }
      
      if (!$this->passwordHasher->check($password, $user->get('pass')->value)) {
        $form_state->setError($form['password'], $this->t('Password Incorrect'));
        $this->logger->info(
          $this->t('[@current_user] - Password Incorrect - @username',[
            '@current_user' => $this->current_user->id(),
            '@username' => $username]));
      }
    } else {
      $form_state->setError($form['username'], $this->t('Username not found.'));
      $this->logger->info(
        $this->t('[@current_user] - Password Incorrect - @username',[
          '@current_user' => $this->current_user->id(),
          '@username' => $username]));
    }
  }
  
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $username = $form_state->getValue('username');
    $enterprise = $form_state->get('enterprise');
    
    $users = $this->efichajesDatabase->getUserByUsername($username);
    
    $user = current($users);
    user_login_finalize($user);
    
    $this->logger->info(
      $this->t('[@current_user] - Login OK - @username',[
        '@current_user' => $this->current_user->id(),
        '@username' => $username]));
    
    $form_state->setRedirect('efichajes.signing', [
      'node' => $enterprise->id(),
      'user' => $user->id()]);
  }
}