<?php

namespace Drupal\efichajes\Form;

use Drupal\Core\Form\FormBase;
use Drupal\efichajes\EfichajesDatabaseInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\InvokeCommand;
use Drupal\Core\Ajax\CloseModalDialogCommand;

class ModalSigningType extends FormBase {
  protected $efichajesDatabase;
  
  public function __construct(EfichajesDatabaseInterface $efichajesDatabase) {
    $this->efichajesDatabase = $efichajesDatabase;
  }
  
  public static function create(ContainerInterface $container) {
    return new static (
      $container->get('efichajes.database')
    );
  }
  
  public function getFormId() {
    return 'efichajes_AddSigningType_Form';
  }
  
  public function ajaxSubmit(array &$form, FormStateInterface $form_state) {
    $response = new AjaxResponse();
    $response->addCommand(new CloseModalDialogCommand());
    $response->addCommand(new InvokeCommand('#edit-refresh', 'click'));
    
    return $response;
  }
  
  public function validateForm(array &$form, FormStateInterface $form_state) {
    $description = $form_state->getValue('description');
    if (trim($description) == '') {
      $form_state->setError($form['description'], $this->t('Empty description'));
    }
  }
  
  public function buildForm(array $form, FormStateInterface $form_state, $signing_id = 0, $operation = 'add') {
    $form_state->set('signing_id', $signing_id);
    $form_state->set('operation', $operation);
    
    if ($operation == 'update') {
      $signing = $this->efichajesDatabase->getSigning($signing_id);
      $description = $signing['description'];
      $status = $signing['status'];
    }
    
    $form['form_description'] = [
      '#markup' => $this->t('Use this form to add/update a Signing Type'),
    ];
    
    if ($operation == 'update') {
      $form['signing_id'] = [
        '#type' => 'textfield',
        '#title' => 'Signing Id',
        '#description' => $this->t('Your signing id'),
        '#default_value' => $form_state->get('signing_id'),
      ];
    }
    
    $form['description'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Signing Description'),
      '#description' => $this->t('Intro signing description'),
      '#default_value' => empty($description) ? '' : $description,
    ];
    
    $form['status'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enabled'),
      '#default_value' => empty($status) ? FALSE : TRUE,
    ];
    
    $form['actions'] = [
      '#type' => 'container',
    ];
    
    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Submit'),
    ];
    
    return $form;
  }
  
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $signing_id = $form_state->get('signing_id');
    $operation = $form_state->get('operation');
    $description = trim($form_state->getValue('description'));
    $status = $form_state->getValue('status');
    
    if ($operation == 'add') {
      $this->efichajesDatabase->insertSigning($description, $status);
    } else if ($operation == 'update') {
      $this->efichajesDatabase->updateSigning($signing_id, $description, $status);
    }
    
    $form_state->setRedirect('efichajes.signingstypes');
  }
}