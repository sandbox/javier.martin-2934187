<?php

namespace Drupal\efichajes\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\node\NodeInterface;
use Drupal\user\UserInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RequestStack;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\ReplaceCommand;
use Drupal\efichajes\EfichajesDatabaseInterface;

class Signing extends FormBase {
  protected $requestStack;
  protected $efichajesDatabase;
  
  public function __construct(RequestStack $requestStack, EfichajesDatabaseInterface $efichajesDatabase) {
    $this->requestStack = $requestStack;
    $this->efichajesDatabase = $efichajesDatabase;
  }
  
  public static function create (ContainerInterface $container) {
    return new static (
      $container->get('request_stack'),
      $container->get('efichajes.database')
    );
  }
  
  public function getFormId() {
    return 'efichajes_signing_form';
  }
  
  /**
   * Return all signings type enabled for an enterprise
   * @param int $nid
   * @return array
   */
  protected function getSigningstype($nid) {
    $result = $this->efichajesDatabase->getEnterpriseSigningstype($nid);
    $signings = [];
    foreach ($result as $key => $value) {
      $signings[$key] = $key . ' - ' . $value['description'];
    }
    
    return $signings;
  }
  
  /**
   * Return signings for user/enterprise current date.
   * @param int $nid
   * @param int $uid
   * @return array
   */
  protected function getSignings($nid, $uid) {
    $current_date = time();
    $result = $this->efichajesDatabase->getDaySignings($nid, $uid, $current_date);
    
    $signings = [];
    foreach ($result as $key => $value) {
      $signings[$key]['signing_date'] = $value['signing_date'];
      $signings[$key]['stid'] = $value['stid'];
      $signings[$key]['description'] = $value['description'];
    }
    
    return $signings;
  }
  
  public function ajaxSigningButton(array &$form, FormStateInterface $form_state) {
    $response = new AjaxResponse();
    
    $response->addCommand(new ReplaceCommand('#signing-table', render($form['signing'])));
    return $response;
  }
  
  public function buildForm(array $form, FormStateInterface $form_state, 
      NodeInterface $node = NULL, UserInterface $user = NULL) {
    $nid = $node->id();
    $uid = $user->id();
    
    // Only if all url parameters are set and user belong enterprise
    // show form.
    if (!empty($node) && !empty($user) && $this->efichajesDatabase->isUserEnterprise($uid, $nid)) {
      $element = $form_state->getTriggeringElement();
      if (!empty($element) && $element['#name'] == 'signingbutton') {
        $request = $this->requestStack->getCurrentRequest();
        $server = $request->server;
        $parameters = $server->all();
        
        $ip_client = $parameters['HTTP_X_FORWARDED_FOR'];
        $browser_info = $parameters['HTTP_USER_AGENT'];
        $stid = $form_state->getValue('signing_type');
        $nid = $node->id();
        $uid = $user->id();
        
        $this->efichajesDatabase->insertWorkerSigning($nid, $uid, $stid, $ip_client, $browser_info);
      }
      
      $form['#attached']['library'][] = 'efichajes/timeclock';
      
      $form['enterprise'] = [
        '#markup' => $node->getTitle(),
        '#prefix' => '<h2>',
        '#suffix' => '</h2>',
        '#weight' => 1,
      ];
      
      $form['user'] = [
        '#markup' => 
          $user->get('field_worker_surnames')->value . ', ' . 
          $user->get('field_worker_name')->value,
        '#prefix' => '<h3>',
        '#suffix' => '</h3>',
        '#weight' => 2,
      ];
      
      $form['time'] = [
        '#markup' => date('H:i:s'),
        '#prefix' => '<h3 id="timeclock">',
        '#suffix' => '</h3>',
        '#weight' => 3,
      ];
      
      $form['signing_type'] = [
        '#type' => 'select',
        '#title' => $this->t('Signing type'),
        '#description' => $this->t('Select signing type'),
        '#options' => $this->getSigningstype($node->id()),
        '#weight' => 4,
      ];
      
      $form['actions'] = [
        '#type' => 'actions',
        '#weight' => 5,
      ];
      
      $form['actions']['submit'] = [
        '#type' => 'button',
        '#name' => 'signingbutton',
        '#value' => $this->t('Submit'),
        '#ajax' => [
          'callback' => [$this, 'ajaxSigningButton'],
        ],
      ];
      
      $header = [
        $this->t('Date'),
        $this->t('Id'),
        $this->t('Signing Type'),
      ];
      
      $form['signing'] = [
        '#type' => 'table',
        '#caption' => $this->t('Signings'),
        '#header' => $header,
        '#empty' => $this->t('No signing today.'),
        '#rows' => $this->getSignings($node->id(), $user->id()),
        '#weight' => 6,
        '#attributes' => ['id' => 'signing-table'],
      ];
    }
    else {
      $form['enterprise'] = [
          '#markup' => $this->t('Error: User not assigned to Enterprise.'),
          '#prefix' => '<h2>',
          '#suffix' => '</h2>',
          '#weight' => 1,
      ];
    }
    
    return $form;
  }
  
  public function submitForm(array &$form, FormStateInterface $form_state) { }
}