<?php

namespace Drupal\efichajes\Form;


use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\node\NodeInterface;
use Drupal\user\UserInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Database\Connection;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Psr\Log\LoggerInterface;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\ReplaceCommand;
use Drupal\Core\Ajax\OpenModalDialogCommand;
use Drupal\Core\Form\FormBuilderInterface;
use Drupal\Core\Datetime\DrupalDateTime;

class SigningsIssue extends FormBase {
  protected $node;
  protected $user;
  protected $current_user;
  protected $database;
  protected $entityTypeManager;
  protected $logger;
  protected $form_builder;
  protected $form_date;
  
  public function __construct(AccountInterface $current_user,
    Connection $database, EntityTypeManagerInterface $entityTypeManager,
    LoggerInterface $logger, FormBuilderInterface $form_builder) {
      $this->current_user = $current_user;
      $this->database = $database;
      $this->entityTypeManager = $entityTypeManager;
      $this->logger = $logger;
      $this->form_builder = $form_builder;
  }
  
  public static function create (ContainerInterface $container) {
    return new static (
      $container->get('current_user'),
      $container->get('database'),
      $container->get('entity_type.manager'),
      $container->get('logger.factory')->get('efichajes - SigningsIssue'),
      $container->get('form_builder')
    );
  }
  
  protected function isUserEnterprise($uid, $nid) {
    $query = $this->database
    ->select('efichajes_enterprise_users', 'e')
    ->condition('nid', $nid)
    ->condition('uid', $uid);
  
    $count = $query->countQuery()->execute()->fetchField();
  
    if ($count != 0) return true;
    else return false;
  }
  
  protected function accessForm(NodeInterface $node, UserInterface $user) {
    if (  $user->hasPermission('efichajes module admin') || 
          $this->current_user->id() == 1) {
      return true;
    } else if ( $this->isUserEnterprise($user->id(), $node->id()) &&
                $user->id() == $this->current_user->id() ) {
      return true;              
    }
    
    return false;
  }
  
  public function getFormId() {
    return 'efichajes_signingsissue_form';
  }
  
  protected function getSigningsDay($nid, $uid, $start_date, $end_date) {
    $query = $this->database->select('efichajes_signings', 'a');
    $query->condition('a.signing_date', $start_date, '>=');
    $query->condition('a.signing_date', $end_date, '<=');
    $query->condition('a.nid', $nid, '=');
    $query->condition('a.uid', $uid, '=');
    $query->leftJoin('efichajes_signings_type', 'b', 'a.stid = b.stid');
    $query->fields('a', ['signing_date', 'sid']);
    $query->fields('b', ['stid', 'description']);
    $query->orderBy('a.signing_date', 'ASC');
    
    $result = $query->execute()->fetchAllAssoc('signing_date', \PDO::FETCH_ASSOC);
    return $result;
  }
  
  protected function getSignings($nid, $uid, \DateTime $date) {
    $start_date = clone $date;
    $start_date->setTime('0', '0', '0');
    $end_date = clone $date;
    $end_date->setTime('23', '59', '59');
    
    $signings_day = $this->getSigningsDay(  $nid, 
                                            $uid, 
                                            $start_date->getTimestamp(), 
                                            $end_date->getTimestamp());
    $signings_list = [];
    
    foreach ($signings_day as $key => $value) {
      $signings_list[$value['sid']]['signing'] = [
        '#markup' => date('H:i:s', $value['signing_date']) . ' - ' . 
          $value['description'], 
      ];
      
      $signings_list[$value['sid']]['alter_link'] = [
        '#markup' => 'alter link', 
      ];
      
      $signings_list[$value['sid']]['delete_link'] = [
        '#markup' => 'delete link', 
      ];
    }
    
    return $signings_list;
  }
  
  public function ajaxUpdate(array &$form, FormStateInterface $form_state) {
    $response = new AjaxResponse();
    $response->addCommand(new ReplaceCommand('#signings-table', render($form['signings'])));
    
    return $response;
  }
  
  public function ajaxAdd(array &$form, FormStateInterface $form_state) {
    $node = $this->node;
    $user = $this->user;
    $date_value = $form_state->getValue('date');
    $date = new DrupalDateTime($date_value);
    
    $form = $this->form_builder->getForm('Drupal\efichajes\Form\AddSigningIssue', $node, $user, $date);
    $response = new AjaxResponse();
    $response->addCommand(new OpenModalDialogCommand($this->t('Add Signing'), render($form)));
    
    return $response;
  }
  
  public function buildForm(array $form, FormStateInterface $form_state, 
      NodeInterface $node = NULL, UserInterface $user = NULL) {
    $this->node = $node;
    $this->user = $user;
    if ($this->accessForm($node, $user)) { 
      $element = $form_state->getTriggeringElement();
      if (empty($element)) {
        $date = new \DateTime('now');
      } else {
        $date = new \DateTime($form_state->getValue('date'));
      }
      
      $form['#attached']['library'][] = 'core/drupal.dialog.ajax';
      
      $form['form_description'] = [
        '#markup' => $this->t('Use this form to alter your signigs. 
            Any alteration must be supervised by your enterprise administrator') . '.',
        '#prefix' => '<p>',
        '#suffix' => '</p>',
        '#weight' => 1,
      ];
      
      $form['enterprise'] = [
        '#markup' => $node->getTitle(),
        '#prefix' => '<h2>',
        '#suffix' => '</h2>',
        '#weight' => 2,
      ];
      
      $form['user'] = [
        '#markup' =>
          $user->get('field_worker_surnames')->value . ', ' .
          $user->get('field_worker_name')->value,
        '#prefix' => '<h3>',
        '#suffix' => '</h3>',
        '#weight' => 3,
      ];
      
      $form['date'] = [
        '#type' => 'date',
        '#title' => $this->t('Date'),
        '#default_value' => $date->format('Y-m-d'),
        '#description' => $this->t('Select signings date.'),
        '#weight' => 4,
      ];
      
      $form['actions_date'] = [
        '#type' => 'actions',
        '#weight' => 5,
      ];
      
      $form['actions_date']['update'] = [
        '#type' => 'button',
        '#value' => $this->t('Update'),
        '#name' => 'update',
        '#ajax' => [
          'callback' => [$this, 'ajaxUpdate']
        ],
      ];
      
      $header_signings = [
        $this->t('Signing'),
        $this->t('Alter'),
        $this->t('Delete'),
      ];
      
      $form['signings'] = [
        '#type' => 'table',
        '#caption' => $this->t('Signings'),
        '#header' => $header_signings,
        '#empty' => $this->t('No signing today.'),
        '#attributes' => ['id' => 'signings-table'],
        '#weight' => 6,
      ];
      
      $signigs = $this->getSignings($node->id(), $user->id(), $date);
      foreach ($signigs as $key => $value) {
        $form['signings'][$key] = $value;
      }
      
      $form['actions'] = [
        '#type' => 'actions',
        '#weight' => 7,
      ];
      
      $form['actions']['add'] = [
        '#type' => 'button',
        '#value' => $this->t('Add Issue'),
        '#name' => 'add',
        '#ajax' => [
          'callback' => [$this, 'ajaxAdd'],
        ],
      ];
      
      $header_issues = [
        $this->t('Original Signing'),
        $this->t('Operation'),
        $this->t('Final Signing'),
        $this->t('Status'),
      ];
      
      $form['issues'] = [
        '#type' => 'table',
        '#caption' => $this->t('Issues'),
        '#header' => $header_issues,
        '#empty' => $this->t('No issues available.'),
        '#attributes' => ['id' => 'issues-table'],
        '#rows' => null,
        '#weight' => 8,
      ];
    } else {
      $form['form_description'] = [
        '#markup' => $this->t('Access Denied') . '.',
        '#prefix' => '<p>',
        '#suffix' => '</p>',
      ];
    }
    
    return $form;
  }
  
  public function submitForm(array &$form, FormStateInterface $form_state) { }
}