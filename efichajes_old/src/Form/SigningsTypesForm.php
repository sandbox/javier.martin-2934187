<?php

namespace Drupal\efichajes\Form;


use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\efichajes\EfichajesDatabaseInterface;
use Drupal\Core\Url;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\ReplaceCommand;

class SigningsTypesForm extends FormBase {
  protected $efichajesDatabase;
  
  public function __construct(EfichajesDatabaseInterface $efichajesDatabase) {
    $this->efichajesDatabase = $efichajesDatabase;
  }
  
  public static function create (ContainerInterface $container) {
    return new static (
      $container->get('efichajes.database')
    );
  }
  
  /**
   * Return true if any signing is checked on tableselect.
   * @param FormStateInterface $form_state
   * @return number
   */
  protected function haveValueCheckTS(FormStateInterface $form_state) {
    $ts = $form_state->getValue('signings');
    $bool = 0;
    foreach ($ts as $key => $value) {
      if ($key == $value) {
        $bool = 1;
        break;
      }
    }
    
    return $bool;
  }
  
  public function getFormId() {
    return 'efichajes_signingstypes_form';
  }
  
  /**
   * Return all signings with status field translated
   * @return \Drupal\Core\StringTranslation\TranslatableMarkup
   */
  protected function getSignings() {
    $result = $this->efichajesDatabase->getSignings();
    foreach ($result as $key => $value) {
      $result[$key]['status'] = 
        ($result[$key]['status']) ? $this->t('Enabled') : $this->t('Disabled');
    }
    
    return $result;
  }
  
  public function ajaxClickTableselect(array &$form, FormStateInterface $form_state) {
    $response = new AjaxResponse();
    $response->addCommand(new ReplaceCommand('#action-buttons', $form['actions']));
    
    return $response;
  }
  
  public function ajaxRefresh(array &$form, FormStateInterface $form_state) {
    $response = new AjaxResponse();
    $response->addCommand(new ReplaceCommand('#tableselect-signings', $form['tableselect_container']));
    
    return $response;
  }
  
  public function buildForm(array $form, FormStateInterface $form_state) {
    $element = $form_state->getTriggeringElement();
    if (!empty($element) && ($element['#name'] == 'changestatusbutton')) {
      // ChangeStatus button clicked
      $valueschecked = $form_state->getValue('signings');
      foreach ($valueschecked as $value) {
        // Update signing status
        $this->efichajesDatabase->changeStatusSigning($value);
      }
    } else if (!empty($element) && ($element['#name'] == 'deletebutton')) {
      // Delete button clicked
      $valueschecked = $form_state->getValue('signings');
      foreach ($valueschecked as $value) {
        // Check if signing has been used
        $this->efichajesDatabase->deleteSigning($value);
      }
    } else if (!empty($element) && $element['#name'] == 'addsigningbutton') {
      // Check if description is empty -> do nothing
      // else -> add signing
      $description = $form_state->getValue('add_signing_description');
      $status = $form_state->getValue('add_signing_checkbox');
      if (!empty($description))
        $this->insertSigning($description, $status);
    } else if (!empty($element) && $element['#name'] == 'altersigningbutton') {
      // Update signing
      $stid = $form_state->getValue('alter_signing_id');
      $description = $form_state->getValue('alter_signing_description');
      $status = $form_state->getValue('alter_signing_checkbox');
      
      $this->updateSigning($stid, $description, $status);
    } else if (!empty($element) && $element['#type'] == 'checkbox') {
      // if checked -> get values signing
      // if unchecked -> try to find values for next checked signing
      if ($element['#return_value'] == $element['#default_value']) {
        $form_state->set('signing_id', $element['#default_value']);
      } else {
        if ($this->haveValueCheckTS($form_state)) {
          $ts_values = $form_state->getValue('signings');
          foreach ($ts_values as $key => $value) {
            if ($key == $value) {
              $form_state->set('signing_id', $key);
              break;
            }
          }
        } else {
          $form_state->set('signing_id', null);
        }
      }
    }
    
    $form['#attached']['library'][] = 'core/drupal.dialog.ajax';
    
    $form['form_description'] = [
      '#markup' => $this->t('
        Use this form to create a signing type. Check signing type created
        as "Activated" to allow be asigned to an enterprise.'),
    ];
    
    $form['tableselect_container'] = [
      '#type' => 'container',
      '#attributes' => ['id' => 'tableselect-signings'],
    ];
    
    $header = [
      'stid' => $this->t('Signing Id'),
      'description' => $this->t('Description'),
      'status' => $this->t('Status'),
    ];
    
    $form['tableselect_container']['signings'] = [
      '#type' => 'tableselect',
      '#header' => $header,
      '#options' => $this->getSignings(),
      '#empty' => $this->t('No signings found'),
      '#multiple' => TRUE,
      '#ajax' => [
        'callback' => [$this, 'ajaxClickTableselect'],
      ],
    ];
    
    $form['actions'] = [
      '#type' => 'actions',
      '#attributes' => [
        'id' => ['action-buttons'],
      ],
    ];
    
    $form['actions']['add'] = [
      '#type' => 'link',
      '#name' => 'addbutton',
      '#title' => $this->t('Add'),
      '#url' => Url::fromRoute('efichajes.signingstypesmodal', [
        'signing_id' => 0, 
        'operation' => 'add']),
      '#attributes' => [
        'class' => [
          'use-ajax',
          'button',
        ],
        'data-dialog-type' => 'modal',
      ],
    ];
    
    if (!empty($form_state->get('signing_id'))) {
      $form['actions']['update'] = [
        '#type' => 'link',
        '#name' => 'updatebutton',
        '#title' => $this->t('Update'),
        '#url' => Url::fromRoute('efichajes.signingstypesmodal', [ 
            'signing_id' => $form_state->get('signing_id'), 
            'operation' => 'update']),
        '#attributes' => [
          'class' => [
            'use-ajax',
            'button',
          ],
          'id' => [
            'update-button',
          ],
          'data-dialog-type' => 'modal',
        ],
      ];
    
      $form['actions']['delete'] = [
        '#type' => 'button',
        '#name' => 'deletebutton',
        '#value' => $this->t('Delete'),
        '#ajax' => [
          'callback' => [$this, 'ajaxRefresh'],
        ],
      ];
      
      $form['actions']['changestatus'] = [
        '#type' => 'button',
        '#name' => 'changestatusbutton',
        '#value' => $this->t('Change Status'),
        '#ajax' => [
          'callback' => [$this, 'ajaxRefresh'],
        ],
      ];
    }
    
    return $form;
  }
  
  public function submitForm(array &$form, FormStateInterface $form_state) { }
}