<?php 

namespace Drupal\efichajes\Form;


use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Session\AccountInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Psr\Log\LoggerInterface;
use Drupal\user\UserInterface;
use Drupal\Core\Password\PasswordInterface;

class WorkerChangePassword extends FormBase {
  protected $current_user;
  protected $logger;
  protected $passwordHasher;
  
  public function __construct(AccountInterface $current_user,
      LoggerInterface $logger, PasswordInterface $password_hasher) {
    $this->current_user = $current_user;
    $this->logger = $logger;
    $this->passwordHasher = $password_hasher;
  }
  
  public static function create (ContainerInterface $container) {
    return new static (
      $container->get('current_user'),
      $container->get('logger.factory')->get('efichajes - WorkerChangePasswordForm'),
      $container->get('password')
    );
  }
  
  public function validateForm(array &$form, FormStateInterface $form_state) {
    $user = $form_state->get('user');
    $password = $form_state->getValue('actual_password');
    $user_password = $user->getPassword();
    
    if (!$this->passwordHasher->check($password, $user_password)) {
      $form_state->setError($form['actual_password'], $this->t('Password Incorrect'));
    }
  }
  
  public function getFormId() {
    return 'efichajes_workerchangepassword_form';
  }
  
  public function buildForm(array $form, FormStateInterface $form_state, 
      UserInterface $user = NULL) {
    
    if (!empty($user)) {
      $form_state->set('user', $user);
      
      $form['form_description'] = [
        '#markup' => $this->t('Use this form in order to change your password.')
      ];
      
      $form['actual_password'] = [
        '#type' => 'password',
        '#title' => $this->t('Actual Password'),
        '#description' => $this->t('Intro your actual password'),
        '#maxlength' => 10,
      ];
      
      $form['new_password'] = [
        '#type' => 'password_confirm',
        '#title' => $this->t('New Password'),
        '#description' => $this->t('Intro a new password'),
        '#maxlegth' => 10,
      ];
      
      $form['actions'] = [
        '#type' => 'actions',
      ];
      
      $form['actions']['submit'] = [
        '#type' => 'submit',
        '#value' => $this->t('Submit'),
      ];
    } else {
      $form['error'] = [
        '#markup' => $this->t('User Error'),
      ];
    }
    
    return $form;
  }
  
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $user = $form_state->get('user');
    $new_password = $form_state->getValue('new_password');
    
    $user->setPassword($new_password);
    $user->save();
    
    $this->logger->info(
      $this->t('Actual User: @uid. Password Changed. Uid: @uid_passwordchanged', [
        '@uid' => $this->current_user->id(),
        '@uid_passwordchanged' => $user->id(),
      ])
    );
    
    drupal_set_message($this->t('Password change successfully'));
  }

}