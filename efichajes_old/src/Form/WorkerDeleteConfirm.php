<?php

namespace Drupal\efichajes\Form;

use Drupal\Core\Form\ConfirmFormBase;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Database\Connection;
use Psr\Log\LoggerInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\user\UserInterface;
use Drupal\Core\Url;
use Symfony\Component\DependencyInjection\ContainerInterface;

class WorkerDeleteConfirm extends ConfirmFormBase {
  protected $current_user;
  protected $database;
  protected $entityTypeManager;
  protected $logger;
  protected $user;
  
  public function __construct(AccountInterface $current_user,
      Connection $database, EntityTypeManagerInterface $entityTypeManager,
      LoggerInterface $logger) {
        $this->current_user = $current_user;
        $this->database = $database;
        $this->entityTypeManager = $entityTypeManager;
        $this->logger = $logger;
  }
  
  public static function create (ContainerInterface $container) {
    return new static (
      $container->get('current_user'),
      $container->get('database'),
      $container->get('entity_type.manager'),
      $container->get('logger.factory')->get('efichajes - WorkerDeleteConfirm')
    );
  }
  
  public function getFormId() {
    return 'efichajes_workerdeleteconfirm_form';
  }
  
  protected function deleteWorkerEnterprises($uid) {
    $query = $this->database->delete('efichajes_enterprise_users');
    $query->condition('uid', $uid, '=');
    $query->execute();
  }
  
  protected function deleteWorkerSignings($uid) {
    $query = $this->database->delete('efichajes_signings');
    $query->condition('uid', $uid, '=');
    $query->execute();
  }
  
  public function buildForm(array $form, FormStateInterface $form_state, UserInterface $user = NULL) {
    $this->user = $user;
    return parent::buildForm($form, $form_state);
  }
  
  public function getQuestion() {
    $full_name = 
      $this->user->get('field_worker_surnames')->value . ', ' . $this->user->get('field_worker_name')->value;
    
    return $this->t('Are you sure do you want to delete user @full_name? 
      This action will delete this user from system, his relations with enterprises and signings.', [
      '@full_name' => $full_name,
    ]);
  }
  
  public function getConfirmText() {
    return $this->t('Delete');
  }
  
  public function getCancelText() {
    return $this->t('Cancel');
  }
  
  public function getCancelUrl() {
    $url = Url::fromRoute('efichajes.workers_admin');
    return $url;
  }
  
  public function validateForm(array &$form, FormStateInterface $form_state) {
    if (!$this->user->hasPermission('eFichajes worker')) {
      $form_state->setErrorByName(
        'User', 
        $this->t('Sorry, using this form you can only delete user with \'efichajes worker\' permission.')
      );
    }
  }
  
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $uid = $this->user->id();
    $this->deleteWorkerEnterprises($uid);
    $this->deleteWorkerSignings($uid);
    $this->user->delete();
    
    drupal_set_message($this->t('User enterprise relations deleted.'));
    drupal_set_message($this->t('User signings deleted.'));
    drupal_set_message($this->t('User deleted.'));
    
    $form_state->setRedirectUrl($this->getCancelUrl());
  }
}