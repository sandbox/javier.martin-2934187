<?php

namespace Drupal\efichajes\Form;


use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\node\NodeInterface;
use Drupal\user\UserInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Database\Connection;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\ReplaceCommand;

class WorkerSigningsList extends FormBase {
  protected $current_user;
  protected $database;
  protected $entityTypeManager;
  protected $logger;
  
  public function __construct(AccountInterface $current_user,
    Connection $database, EntityTypeManagerInterface $entityTypeManager,
    LoggerInterface $logger) {
      $this->current_user = $current_user;
      $this->database = $database;
      $this->entityTypeManager = $entityTypeManager;
      $this->logger = $logger;
  }
  
  public static function create (ContainerInterface $container) {
    return new static (
      $container->get('current_user'),
      $container->get('database'),
      $container->get('entity_type.manager'),
      $container->get('logger.factory')->get('efichajes - WorkerSigningList')
    );
  }
  
  public function getFormId() {
    return 'efichajes_workersigninglist_form';
  }
  
  protected function isUserEnterprise($uid, $nid) {
    $query = $this->database
    ->select('efichajes_enterprise_users', 'e')
    ->condition('nid', $nid)
    ->condition('uid', $uid);
  
    $count = $query->countQuery()->execute()->fetchField();
  
    if ($count != 0) return true;
    else return false;
  }
  
  protected function secstohours($sec) {
    $hours = floor($sec / 3600);
    $min = floor(($sec - ($hours * 3600)) / 60);
    $sec = $sec - ($hours * 3600) - ($min * 60);
    
    return $hours . ' h:' . $min . ' m:' . $sec . ' s';
  }
  
  protected function getWorkTime(array $signings_day) {
    $ini = 1;
    $temp = 0;
    $sum = 0;
    foreach ($signings_day as $value) {
      if ($ini) {
        $temp = $value['signing_date'];
        $ini = 0;
      } else {
        $sum = $sum + $value['signing_date'] - $temp;
        $ini = 1;
      }
    }
    
    return $sum;
  }
  
  protected function getSigningsDay($nid, $uid, $start_date, $end_date) {
    $query = $this->database->select('efichajes_signings', 'a');
    $query->condition('a.signing_date', $start_date, '>=');
    $query->condition('a.signing_date', $end_date, '<=');
    $query->condition('a.nid', $nid, '=');
    $query->condition('a.uid', $uid, '=');
    $query->leftJoin('efichajes_signings_type', 'b', 'a.stid = b.stid');
    $query->fields('a', ['signing_date']);
    $query->fields('b', ['stid', 'description']);
    $query->orderBy('a.signing_date', 'ASC');
    
    $result = $query->execute()->fetchAllAssoc('signing_date', \PDO::FETCH_ASSOC);
    return $result;
  }
  
  protected function getSignings($nid, $uid, \DateTime $start_date, \DateTime $end_date,
      FormStateInterface $form_state) {
    $signings = [];
    $timeatwork = 0;
    
    while($start_date <= $end_date) {
      $key = $start_date->format('Y-m-d');
      $date_ini = $start_date->getTimestamp();
      $date_end = strtotime('+1 days', $start_date->getTimestamp());
      
      $signings_day = $this->getSigningsDay($nid, $uid, $date_ini, $date_end);
      $signings_list = [];
      foreach($signings_day as $value) {
        $date_signing = date('H:i:s', $value['signing_date']);
        $signings_list[] = [
          '#markup' => $date_signing . ' ' . $value['description'] . '<br>',
        ];
      }
      
      $signings[$key]['date'] = [
        '#markup' => date('d/m/Y', $start_date->getTimestamp()),
      ];
      $signings[$key]['signing'] = $signings_list;
      
      $daytime = $this->getWorkTime($signings_day);
      $timeatwork = $timeatwork + $daytime;
      
      $signings[$key]['hours'] = [
        '#markup' => $this->secstohours($daytime),
      ];
      $signings[$key]['workday time'] = [
        '#markup' => 0,
      ];
      $signings[$key]['differences'] = [
        '#markup' => 0,
      ];
      
      $start_date->modify('+1 day');
    }
    $form_state->set('timeatwork', $timeatwork);
    
    return $signings;
  }
  
  public function ajaxRefreshButton(array &$form, FormStateInterface $form_state) {
    $response = new AjaxResponse();
    $response->addCommand(new ReplaceCommand('#signings-table', render($form['signings'])));
    $response->addCommand(new ReplaceCommand('#time-at-work', render($form['timeatwork'])));
    
    return $response;
  }
  
  public function buildForm(array $form, FormStateInterface $form_state, NodeInterface $node = NULL,
      UserInterface $user = NULL) {
    $nid = $node->id();
    $uid = $user->id();
    
    if (!empty($node) && !empty($user) && $this->isUserEnterprise($uid, $nid)) {
      $element = $form_state->getTriggeringElement();
      if (!empty($element)) {
        $start_date = new \DateTime($form_state->getValue('start_date'));
        $start_date->setTime('0', '0', '0');
        $end_date = new \DateTime($form_state->getValue('end_date'));
        $end_date->setTime('23', '59', '59');
      } else {
        $start_date = new \DateTime('now');
        $end_date = new \DateTime('now');
        
        $start_date->modify('first day of this month');
        $start_date->setTime('0', '0', '0');
        $end_date->modify('last day of this month');
        $end_date->setTime('23', '59', '59');
      }
      
      $form['form_description'] = [
        '#markup' => $this->t('Use this form to see your signings.'),
        '#weight' => 1,
      ];
      
      $form['enterprise'] = [
        '#markup' => $node->getTitle(),
        '#prefix' => '<h2>',
        '#suffix' => '</h2>',
        '#weight' => 2,
      ];
      
      $form['user'] = [
        '#markup' =>
          $user->get('field_worker_surnames')->value . ', ' .
          $user->get('field_worker_name')->value,
        '#prefix' => '<h3>',
        '#suffix' => '</h3>',
        '#weight' => 3,
      ];
      
      $form['start_date'] = [
        '#type' => 'date',
        '#title' => $this->t('Start Date'),
        '#default_value' => $start_date->format('Y-m-d'),
        '#weight' => 4,
      ];
      
      $form['end_date'] = [
        '#type' => 'date',
        '#title' => $this->t('End Date'),
        '#default_value' => $end_date->format('Y-m-d'),
        '#weight' => 5,
      ];
      
      $form['actions'] = [
        '#type' => 'actions',
        '#weight' => 6,
      ];
      
      $form['actions']['refresh'] = [
        '#type' => 'button',
        '#name' => 'refreshbutton',
        '#value' => $this->t('Refresh'),
        '#ajax' => [
          'callback' => [$this, 'ajaxRefreshButton'],
        ],
      ];
      
      $header = [
        $this->t('Date'),
        $this->t('Signing'),
        $this->t('Hours'),
        $this->t('WorkDay Time'),
        $this->t('Differences'),
      ];
      
      $form['signings'] = [
        '#type' => 'table',
        '#caption' => $this->t('Signings'),
        '#header' => $header,
        '#empty' => $this->t('No signing today.'),
        '#attributes' => ['id' => 'signings-table'],
        '#weight' => 7,
      ];
      
      $signings = $this->getSignings($nid, $uid, $start_date, $end_date, $form_state);       
      foreach ($signings as $key => $value) {
        $form['signings'][$key] = $value;
      }
      
      $timeatwork = $form_state->get('timeatwork');
      $form['timeatwork'] = [
        '#markup' => 
          $this->t('Total Time at Work') . ': ' . $this->secstohours($timeatwork),
        '#prefix' => '<div id="time-at-work"><p>',
        '#suffix' => '</p></div>',
        '#weight' => 7,
      ];
      
    } else {
      $form['enterprise'] = [
        '#markup' => $this->t('Error: User not assigned to Enterprise.'),
        '#prefix' => '<h2>',
        '#suffix' => '</h2>',
        '#weight' => 1,
      ];
    }
    
    return $form;
  }
  
  public function submitForm(array &$form, FormStateInterface $form_state) { }
}