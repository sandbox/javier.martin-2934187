<?php

namespace Drupal\efichajes\Form;


use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Database\Connection;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Utility\LinkGeneratorInterface;
use Drupal\Core\Url;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\ReplaceCommand;
use Drupal\efichajes\EfichajesDatabaseInterface;

class WorkersAdmin extends FormBase {
  protected $current_user;
  protected $database;
  protected $entityTypeManager;
  protected $logger;
  protected $link_generator;
  protected $efichajes_database;
  
  public function __construct(AccountInterface $current_user,
    Connection $database, EntityTypeManagerInterface $entityTypeManager,
    LoggerInterface $logger, LinkGeneratorInterface $link_generator,
    EfichajesDatabaseInterface $efichajes_database) {
      $this->current_user = $current_user;
      $this->database = $database;
      $this->entityTypeManager = $entityTypeManager;
      $this->logger = $logger;
      $this->link_generator = $link_generator;
      $this->efichajes_database = $efichajes_database;
  }
  
  public static function create (ContainerInterface $container) {
    return new static (
      $container->get('current_user'),
      $container->get('database'),
      $container->get('entity_type.manager'),
      $container->get('logger.factory')->get('efichajes - WorkersAdmin'),
      $container->get('link_generator'),
      $container->get('efichajes.database')
    );
  }
  
  public function getFormId() {
    return 'efichajes_workersmaintenance_form';
  }
  
  protected function getEnterpriseWorkers($nid) {
    $query = $this->database->select('efichajes_enterprise_users', 'a');
    $query->fields('a',['uid']);
    $query->condition('a.nid', $nid, '=');
    
    $workers = $query->execute()->fetchAllAssoc('uid', \PDO::FETCH_ASSOC);
    $user_storage = $this->entityTypeManager->getStorage('user');
    $result = [];
    foreach ($workers as $key => $value) {
      $user = $user_storage->load($key);
      
      $result[$key]['login'] = $user->get('name')->value;
      $result[$key]['fullname'] = 
      $user->get('field_worker_surnames')->value . ', ' . $user->get('field_worker_name')->value;
      
      $url = Url::fromRoute('efichajes.worker_signings', [
        'node' => $nid,
        'user' => $user->id()]);
      $link = $this->link_generator->generate($this->t('My Signings'), $url);
      $result[$key]['signings'] = $link;
      
      $url = Url::fromRoute('efichajes.worker_delete', [
        'user' => $user->id()]);
      $link = $this->link_generator->generate($this->t('Delete'), $url);
      $result[$key]['delete'] = $link;
    } 
    
    return $result;
  }
  
  public function ajaxEnterprise(array &$form, FormStateInterface $form_state) {
    $response = new AjaxResponse();
    
    $response->addCommand(new ReplaceCommand('#workers-table', render($form['signings'])));
    return $response;
  }
  
  public function buildForm(array $form, FormStateInterface $form_state) {
    $element = $form_state->getTriggeringElement();
    if (empty($element)) {
      $node_storage = $this->entityTypeManager->getStorage('node');
      $uid = $this->current_user->id();
      $all_enterprises = $node_storage->loadByProperties([
        'type' => 'enterprise',
        'status' => 1,
      ]);
      
      $enterprises = [];
      foreach ($all_enterprises as $key => $value) {
        if ($uid == 1 || $this->efichajes_database->isUserEnterprise($uid, $key) ) {
          $enterprises[$key] = $value->getTitle();
        }
      }
      
      reset($enterprises);
      $enterprise = key($enterprises);
    } else {
      $enterprise = $form_state->getValue('enterprise');
    }
    
    $form['form_description'] = [
      '#markup' => $this->t('Use this form to administer workers.'),
    ];
     
    $form['enterprise'] = [
      '#type' => 'select',
      '#title' => $this->t('Enterprise'),
      '#description' => $this->t('Select an enterprise'),
      '#options' => $enterprises,
      '#default_option' => $enterprise,
      '#ajax' => [
        'callback' => [ $this, 'ajaxEnterprise'],
      ],
    ];
    
    $header = [
      $this->t('login'),
      $this->t('Full Name'),
      $this->t('Signings'),
      $this->t('Delete')
    ];
      
    $form['signings'] = [
      '#type' => 'table',
      '#caption' => $this->t('Workers List'),
      '#header' => $header,
      '#empty' => $this->t('No Workers in this enterprise.'),
      '#attributes' => ['id' => 'workers-table'],
      '#rows' => $this->getEnterpriseWorkers($enterprise),
    ];
    
    return $form;
  }
  
  public function submitForm(array &$form, FormStateInterface $form_state) { }
}