<?php

namespace Drupal\efichajes\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Utility\LinkGeneratorInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\Url;
use Drupal\Core\Database\Connection;
use Drupal\Core\Entity\EntityTypeManagerInterface;

/**
 * 
 * Provides Worker Block with links
 * 
 * @Block (
 *  id = "efichajes_worker_block",
 *  admin_label = @Translation("eFichajes Workers Block")
 * )
 */

class WorkerLinkBlock extends BlockBase implements ContainerFactoryPluginInterface { 
  protected $link_generator;
  protected $current_route;
  protected $database;
  protected $entityTypeManager;
  
  public function __construct(
      array $configuration, 
      $plugin_id, 
      $plugin_definition,
      LinkGeneratorInterface $link_generator,
      RouteMatchInterface $current_route,
      Connection $database,
      EntityTypeManagerInterface $entityTypeManager) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->link_generator = $link_generator;
    $this->current_route = $current_route;
    $this->database = $database;
    $this->entityTypeManager = $entityTypeManager;
  }
  
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static (
      $configuration, 
      $plugin_id,
      $plugin_definition,
      $container->get('link_generator'),
      $container->get('current_route_match'),
      $container->get('database'),
      $container->get('entity_type.manager')
    );
  }
  
  protected function getUserEnterprise($uid) {
    $query = $this->database->select('efichajes_enterprise_users','a');
    $query->fields('a', ['nid']);
    $query->condition('a.uid', $uid, '=');
    
    $result = $query->execute()->fetchAllAssoc('nid', \PDO::FETCH_ASSOC);
    
    return $result;
  }
  
  public function build() {
    $user = $this->current_route->getParameter('user');
    $enterprises = $this->getUserEnterprise($user->id());
    $storage = $this->entityTypeManager->getStorage('node');
    $form = [];
    
    foreach ($enterprises as $value) {
      $enterprise = $storage->load($value['nid']);
      $enterprise_name = $enterprise->getTitle();
      
      $url = Url::fromRoute('efichajes.worker_signings', [
        'node' => $value['nid'],
        'user' => $user->id()]);
      $link = $this->link_generator->generate($this->t('@enterprise - My Signings', [
        '@enterprise' => $enterprise_name,
      ]), $url);
      $form[$value['nid'] . '-signings'] = [
        '#markup' => $link,
        '#prefix' => '<div id="link-signings">',
        '#suffix' => '</div>',
      ];
      
      $url = Url::fromRoute('efichajes.signing', [
        'node' => $value['nid'],
        'user' => $user->id()]);
      $link = $this->link_generator->generate($this->t('@enterprise - TimeClock', [
        '@enterprise' => $enterprise_name,
      ]), $url);
      $form[$value['nid'] . '-timeclock'] = [
        '#markup' => $link,
        '#prefix' => '<div id="link-timeclock">',
        '#suffix' => '</div>',
      ];
    }
    
    $url = Url::fromRoute('efichajes.worker_changepassword', [
      'user' => $user->id()]);
    $link = $this->link_generator->generate($this->t('Change Password'), $url);
    
    $form['link_changepassword'] = [
      '#markup' => $link,
      '#prefix' => '<div id="link-changepassword">',
      '#suffix' => '</div>',
    ];
    
    return $form;
  }
}