#!/bin/sh

#Move to directory
cd /var/www/drupal8

#Export user fields
./vendor/bin/drupal config:export:single --name=field.field.user.user.field_efichajes_user_id --module=efichajes --include-dependencies --remove-uuid --remove-config-hash --optional
./vendor/bin/drupal config:export:single --name=field.field.user.user.field_efichajes_user_name --module=efichajes --include-dependencies --remove-uuid --remove-config-hash --optional
./vendor/bin/drupal config:export:single --name=field.field.user.user.field_efichajes_user_surname --module=efichajes --include-dependencies --remove-uuid --remove-config-hash --optional
./vendor/bin/drupal config:export:single --name=field.field.user.user.field_efichajes_user_phone --module=efichajes --include-dependencies --remove-uuid --remove-config-hash --optional

#Export content type
./vendor/bin/drupal config:export:content:type --module=efichajes --optional-config=yes --remove-uuid --remove-config-hash signingtype
./vendor/bin/drupal config:export:content:type --module=efichajes --optional-config=yes --remove-uuid --remove-config-hash signing